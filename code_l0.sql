--Для новой базы в папке redoute выполнить
--psql -h 10.54.3.14 -U postgres -d catalog_l0 < code_l0.sql
--psql -d dev1_catalog_l0 -h 10.54.9.232 -U postgres < code_work.sql
-- Каталог Л0
drop table if exists  public.code_input;
CREATE TABLE IF NOT EXISTS public.code_input
(
    code smallint NOT NULL,
	brief varchar(50),
	note varchar(256),
    CONSTRAINT code_input_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_input(code, brief)	VALUES (0, 'RAW');
INSERT INTO public.code_input(code, brief)	VALUES (1, 'L0');
INSERT INTO public.code_input(code, brief)	VALUES (255, 'UNDEF');

ALTER TABLE "PM" add "in_params_pullUpToRef" BOOLEAN;
create view tmp_border as select "productId" as product,
concat(
	'(',
		'(',"geoLocation_topLeft_val"[1], ',' , "geoLocation_topLeft_val"[2], ')', ',',
		'(',"geoLocation_topRight_val"[1], ',' , "geoLocation_topRight_val"[2], ')', ',',
		'(',"geoLocation_bottomRight_val"[1], ',' , "geoLocation_bottomRight_val"[2], ')', ',',
		'(',"geoLocation_bottomLeft_val"[1], ',' , "geoLocation_bottomLeft_val"[2], ')',
	')') as border
from "PM";
update "PM" 
set "geoLocation_geoBorder" = cast (border as polygon) 
from tmp_border
where "productId" = product 


обновление путей в каталоге Л0
update "CatalogItem" 
set path = concat('/srv/l0fs/archive_l0/', substring("productId", 25, 4), '/', substring("productId", 29, 2), '/', substring("productId", 31, 2), '/', "productId" )
where   "productId" like 'RP%KSHMSA_VR2_%'

Версия 25.01.0

ALTER TABLE "MsgPair" add "in_params_pullUpToRef" BOOLEAN;
ALTER TABLE "MsgPair" add "out_appStatus" SMALLINT;