//////////////////////////////////////////////////////////////////////
// redoute  10.54.3.23 (пароль длинный) - tamarinRedoute в виде сервиса
// maker    10.54.9.247     - веб интерфейс к редуту 
//////////////////////////////////////////////////////////////////////

class Production {
  constructor() {
    this.rootConfig =    '/mnt/workfs/apoi/config'
    this.config=         'production'
    this.port =          '8081'        
    this.archiveL0 =     '/srv/l0fs/archive_l0'
    this.archiveL2=      '/srv/l2fs/archive_l2'
    this.tmpDir =        '/mnt/workfs/redoute/tmp'
    this.interim =       '/mnt/workfs/redoute/interim'
    this.output =        '/mnt/workfs/redoute/output'
    this.error =         '/mnt/workfs/redoute/error'
    this.db =            'postgres://postgres:Passw0rd@10.54.24.14:5432/work'
    this.dbL0 =          'postgres://postgres:Passw0rd@10.54.24.14:5432/catalog_l0'
    this.dbL2 =          'postgres://postgres:Passw0rd@10.54.24.14:5432/catalog_l2'
    this.ssh =           'ssh handler@10.54.24.23'

    this.configPath =     this.rootConfig + '/' + this.config
    this.tamarinPath =    '/opt/tamarin/runTamarin.sh'
    this.forpostCmd =     this.tamarinPath + ' tamarinForpost -p -cf ' + this.configPath + '/' + 'configForpost.xml'       
    this.path = {}
    this.path['kit'] =    this.configPath + '/' + 'kit'
    this.path['report'] = this.configPath + '/' + 'report'
    this.path['query'] =  this.configPath + '/' + 'query'
  }
}

// redoute  10.54.9.240 screen -r dev1 dev2 ...
// maker    10.54.9.246 screen -r dev1 dev2 ...
// раббит   http://10.54.3.15:15672
class Dev {

  constructor(config, port) {
    this.redoute =      '/mnt/beegfs/redoute/'
    this.config =       config
    this.port =         port
    
    this.rootConfig =    this.redoute + 'config'
    this.tmpDir =        this.redoute + this.config + '/tmp'
    this.archiveL0 =     this.redoute + this.config + '/archive_l0'
    this.archiveL2=      this.redoute + this.config + '/archive_l2'
    this.interim =       this.redoute + this.config + '/interim'
    this.output =        this.redoute + this.config + '/output'
    this.error =         this.redoute + this.config + '/error'  
    this.db =            'postgres://admin:Passw0rd@10.54.9.232:5432/' + this.config + '_work'
    this.dbL0 =          'postgres://admin:Passw0rd@10.54.9.232:5432/' + this.config + '_catalog_l0'
    this.dbL2 =          'postgres://admin:Passw0rd@10.54.9.232:5432/' + this.config + '_catalog_l2'
    this.ssh =           'ssh handler@10.54.9.80'   
    
    this.configPath =     this.rootConfig + '/' + this.config
    this.tamarinPath =    this.configPath + '/' + 'tamarin/runTamarin.sh'
    this.forpostCmd =     this.tamarinPath + ' tamarinForpost -p -cf ' + this.configPath + '/' + 'configForpost.xml'       
    this.path = {}
    this.path['kit'] =    this.configPath + '/' + 'kit'
    this.path['report'] = this.configPath + '/' + 'report'
    this.path['query'] =  this.configPath + '/' + 'query'
  }
}

// redoute  10.54.9.240 screen -r dev1 dev2 ...
// maker    10.54.9.246 screen -r dev1 dev2 ...
// раббит   http://10.54.3.15:15672
class Devx {

  constructor(config, port) {
    this.redoute =      '/mnt/beegfs/redoute/'
    this.config =       config
    this.port =         port
    
    this.rootConfig =    this.redoute + 'config'
    this.tmpDir =        this.redoute + this.config + '/tmp'
    this.archiveL0 =     this.redoute + this.config + '/archive_l0'
    this.archiveL2=      this.redoute + this.config + '/archive_l2'
    this.interim =       this.redoute + this.config + '/interim'
    this.output =        this.redoute + this.config + '/output'
    this.error =         this.redoute + this.config + '/error'  
    this.db =            'postgres://admin:Passw0rd@10.54.9.232:5432/' + this.config + '_work'
    this.dbL0 =          'postgres://admin:Passw0rd@10.54.9.232:5432/' + this.config + '_catalog_l0'
    this.dbL2 =          'postgres://admin:Passw0rd@10.54.9.232:5432/' + this.config + '_catalog_l2'
    this.ssh =           'ssh handler@10.54.9.80'   
    
    this.configPath =     this.rootConfig + '/' + this.config
    this.tamarinPath =    this.configPath + '/' + 'tamarin/runTamarin.sh'
    this.forpostCmd =     this.tamarinPath + ' tamarinForpost -p -cf ' + this.configPath + '/' + 'configForpost.xml'       
    this.path = {}
    this.path['kit'] =    this.configPath + '/' + 'kit'
    this.path['report'] = this.configPath + '/' + 'report'
    this.path['query'] =  this.configPath + '/' + 'query'
  }
}


// redoute  10.45.4.41 screen -r redoute-tamarin
// maker    10.45.4.41 screen -r redoute-maker
class Archive {

  constructor(config, port) {
    this.redoute =      '/mnt/beegfs/redoute/'
    this.config =       config
    this.port =         port
    
    this.rootConfig =    this.redoute + 'config'
    this.tmpDir =        this.redoute + this.config + '/tmp'
    this.archiveL0 =     this.redoute + this.config + '/archive_l0'
    this.archiveL2=      this.redoute + this.config + '/archive_l2'
    this.interim =       this.redoute + this.config + '/interim'
    this.output =        this.redoute + this.config + '/output'
    this.error =         this.redoute + this.config + '/error'  
    this.db =            'postgres://postgres:postgres@10.45.4.41:5432/' + this.config + '_work'
    this.dbL0 =          'postgres://postgres:postgres@10.45.4.41:5432/' + this.config + '_catalog_l0'
    this.dbL2 =          'postgres://postgres:postgres@10.45.4.41:5432/' + this.config + '_catalog_l2'
    this.ssh =           'ssh handler@10.45.4.41'   
    this.forpostCmd =     this.tamarinPath + ' tamarinForpost -p -cf ' + this.configPath + '/' + 'configForpost.xml'       
    
    this.configPath =     this.rootConfig + '/' + this.config
    this.tamarinPath =    this.configPath + '/' + 'tamarin/runTamarin.sh'

    this.path = {}
    this.path['kit'] =    this.configPath + '/' + 'kit'
    this.path['report'] = this.configPath + '/' + 'report'
    this.path['query'] =  this.configPath + '/' + 'query'
  }
}

// redoute  10.54.3.17(15) 
// maker    10.54.3.17(15) 
class Ffd {

  constructor(config, port) {
    this.redoute =      '/mnt/workfs/redoute/apoi/'
    this.config =       config
    this.port =         port
    
    this.rootConfig =    '/mnt/workfs/apoi/config/'
    this.tmpDir =        '/mnt/workfs/redoute/apoi/tmp'
    this.archiveL0 =     '/mnt/workfs/redoute/apoi/l0'
    this.archiveL2=      '/mnt/workfs/redoute/apoi/l2'
    this.interim =       '/mnt/workfs/redoute/apoi/interim'
    this.output =        '/mnt/workfs/redoute/apoi/output'
    this.error =         this.redoute + this.config + '/error'  
    this.db =            'postgres://postgres:postgres@10.54.24.14:5432/' + this.config + '_work'
    this.dbL0 =          'postgres://postgres:postgres@10.54.24.14:5432/' + this.config + '_catalog_l0'
    this.dbL2 =          'postgres://postgres:postgres@10.54.24.14:5432/' + this.config + '_catalog_l2'
    
    this.configPath =     this.rootConfig + '/' + this.config
    this.ssh =            'ssh handler@10.54.3.23'   
    this.tamarinPath =    '/opt/tamarin/runTamarin.sh'
    this.forpostCmd =     this.tamarinPath + ' tamarinForpost -p -cf ' + this.configPath + '/' + 'configForpost.xml'       

    this.path = {}
    this.path['kit'] =    this.configPath + '/' + 'kit'
    this.path['report'] = this.configPath + '/' + 'report'
    this.path['query'] =  this.configPath + '/' + 'query'
  }
}

// для формирования конфига запуск с сервера 10.54.3.25 
// ./makeDig.sh /opt/dig/tamarin
// sftp://10.54.9.247/mnt/workfs/redoute/config
// redoute 10.54.3.25 service DIGtamarinRedoute
// maker   10.54.3.17 screen -r dig   /opt/dig
class Prod {

  constructor(config, port) {
    this.redoute =      '/mnt/workfs/redoute/'
    this.config =       config
    this.port =         port
    
    this.rootConfig =    this.redoute + 'config'
    this.archiveL0 =     '/srv/l0fs/' + this.config + '/' + 'archive_l0'
    this.archiveL2 =     '/srv/l2fs/' + this.config + '/' + 'archive_l2'
    this.interim =       this.redoute + this.config + '/interim'
    this.output =        this.redoute + this.config + '/output'
    this.error =         this.redoute + this.config + '/error'  
    this.tmpDir =        this.redoute + this.config + '/tmp'
    this.db =            'postgres://postgres:Passw0rd@10.54.24.14:5432/' + this.config + '_work'
    this.dbL0 =          'postgres://postgres:Passw0rd@10.54.24.14:5432/' + this.config + '_catalog_l0'
    this.dbL2 =          'postgres://postgres:Passw0rd@10.54.24.14:5432/' + this.config + '_catalog_l2'
    // ВРЕМЕННО для тестирования на КЦ
    // this.ssh =           'ssh handler@10.54.3.23'   
    this.ssh =           'ssh handler@10.54.3.25'   
    
    this.configPath =     this.rootConfig + '/' + this.config
    this.tamarinPath =    '/opt/' + this.config + '/' + 'tamarin/runTamarin.sh'
    this.forpostCmd =     this.tamarinPath + ' tamarinForpost -p -cf ' + this.configPath + '/' + 'configForpost.xml'       
    this.path = {}
    this.path['kit'] =    this.configPath + '/' + 'kit'
    this.path['report'] = this.configPath + '/' + 'report'
    this.path['query'] =  this.configPath + '/' + 'query'
  }
}



export let dev1 = new Dev('dev1', '8081')
export let dev2 = new Dev('dev2', '8082')
export let dev3 = new Dev('dev3', '8083')
export let dev4 = new Dev('dev4', '8084')
export let dev5 = new Dev('dev5', '8085')
export let dev6 = new Dev('dev6', '8086')
export let dev7 = new Dev('dev7', '8087')
export let dev8 = new Dev('dev8', '8088')
export let dev9 = new Dev('dev9', '8089')

export let arc = new Dev('arc', '8090')

// архив на к4
//export let arc = new Archive('arc', '8081')

export let dev1x = new Devx('dev1x', '8091')
export let dev2x = new Devx('dev2x', '8092')

export let production = new Production()
export let ffd_old = new Ffd('ffd', '8082')

export let dig = new Prod('dig', '8091')

// после проверки при установке в использование ffd_new -> ffd
export let ffd = new Prod('ffd_new', '8092')
