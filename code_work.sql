--Для новой базы в папке redoute выполнить
--psql -h 10.54.3.14 -U postgres -d work < code_work.sql
--psql -d dev1_work -h 10.54.9.80 -U postgres < code_work.sql

drop table if exists public.code_task_status;
CREATE TABLE IF NOT EXISTS public.code_task_status
(
    code smallint NOT NULL,
	brief varchar(50),
	note varchar(256),
    CONSTRAINT code_task_status_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_task_status(code, brief)	VALUES (0, 'NEW_TASK');
INSERT INTO public.code_task_status(code, brief)	VALUES (1, 'NOT_FOUND');
INSERT INTO public.code_task_status(code, brief)	VALUES (2, 'DUPLICATE');
INSERT INTO public.code_task_status(code, brief)	VALUES (3, 'PENDING');
INSERT INTO public.code_task_status(code, brief)	VALUES (4, 'PROCESSING');
INSERT INTO public.code_task_status(code, brief)	VALUES (5, 'DONE');
INSERT INTO public.code_task_status(code, brief)	VALUES (6, 'DELETED');
INSERT INTO public.code_task_status(code, brief)	VALUES (7, 'KILLED');
INSERT INTO public.code_task_status(code, brief)	VALUES (8, 'STOPPED');
INSERT INTO public.code_task_status(code, brief)	VALUES (255, 'UNDEF');


drop table if exists  public.code_job_status;
CREATE TABLE IF NOT EXISTS public.code_job_status
(
    code smallint NOT NULL,
	brief varchar(50),
	note varchar(256),
    CONSTRAINT code_job_status_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_job_status(code, brief)	VALUES (0, 'SUCCESS');
INSERT INTO public.code_job_status(code, brief)	VALUES (1, 'PARTIAL_SUCCESS');
INSERT INTO public.code_job_status(code, brief)	VALUES (2, 'ERROR');
INSERT INTO public.code_job_status(code, brief)	VALUES (255, 'UNDEF');


drop table if exists  public.code_job_exit;
CREATE TABLE IF NOT EXISTS public.code_job_exit
(
    code smallint NOT NULL,
	brief varchar(256),
	note varchar(256),
    CONSTRAINT code_job_exit_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_job_exit(code, brief)	VALUES (0, 'SUCCESS');
INSERT INTO public.code_job_exit(code, brief)	VALUES (3, 'ERROR_COULD_NOT_IMPORT');
INSERT INTO public.code_job_exit(code, brief)	VALUES (4, 'ERROR_PROTECTION');
INSERT INTO public.code_job_exit(code, brief)	VALUES (5, 'ERROR_INVALID_OUTPUT');
INSERT INTO public.code_job_exit(code, brief)	VALUES (6, 'ERROR_INVALID_OPTIONS');
INSERT INTO public.code_job_exit(code, brief)	VALUES (32, 'ERROR_EXITED_ABNORMALLY');
INSERT INTO public.code_job_exit(code, brief)	VALUES (33, 'ERROR_COULD_NOT_RUN');
INSERT INTO public.code_job_exit(code, brief)	VALUES (137, 'SYSERROR 137 - Убит ОС (Падение, память и т.д.)');
INSERT INTO public.code_job_exit(code, brief)	VALUES (143, 'SYSERROR 143 - Убит ОС');
INSERT INTO public.code_job_exit(code, brief)	VALUES (166, 'ERROR_UNDEF');
INSERT INTO public.code_job_exit(code, brief)	VALUES (167, 'ERROR_NO_INTERSECTION_WITH_SHAPE');
INSERT INTO public.code_job_exit(code, brief)	VALUES (168, 'ERROR_COULD_NOT_INIT_RESTORED_SENSOR');
INSERT INTO public.code_job_exit(code, brief)	VALUES (169, 'ERROR_ALL_SERVERS_BUSY_BY_ROOT_NODES');
INSERT INTO public.code_job_exit(code, brief)	VALUES (170, 'ERROR_COULD_NOT_ASSIGN_ROOT_NODE');
INSERT INTO public.code_job_exit(code, brief)	VALUES (171, 'ERROR_COULD_NOT_GENERATE_ANY_SCENE');
INSERT INTO public.code_job_exit(code, brief)	VALUES (172, 'UNDEF');
INSERT INTO public.code_job_exit(code, brief)	VALUES (1, 'SYSERROR 1 Ловушка для общих ошибок');
INSERT INTO public.code_job_exit(code, brief)	VALUES (2, 'SYSERROR 2 Неправильное использование встроенных функций');
INSERT INTO public.code_job_exit(code, brief)	VALUES (65, 'SYSERROR 65 Неправильный формат данных');
INSERT INTO public.code_job_exit(code, brief)	VALUES (66, 'SYSERROR 66 Файл не доступен');
INSERT INTO public.code_job_exit(code, brief)	VALUES (67, 'SYSERROR 67 Пользователь не существует');
INSERT INTO public.code_job_exit(code, brief)	VALUES (68, 'SYSERROR 68 Узел не существует');
INSERT INTO public.code_job_exit(code, brief)	VALUES (69, 'SYSERROR 69 Служба недоступна');
INSERT INTO public.code_job_exit(code, brief)	VALUES (70, 'SYSERROR 70 Внутренная ошибка СПО');
INSERT INTO public.code_job_exit(code, brief)	VALUES (71, 'SYSERROR 71 Ошибка ОС');
INSERT INTO public.code_job_exit(code, brief)	VALUES (72, 'SYSERROR 72 Системный файл не существет');
INSERT INTO public.code_job_exit(code, brief)	VALUES (73, 'SYSERROR 73 Выходной файл не может быть создан');
INSERT INTO public.code_job_exit(code, brief)	VALUES (74, 'SYSERROR 74 Ошибка ввода/вывлда при операциях с файлом');
INSERT INTO public.code_job_exit(code, brief)	VALUES (75, 'SYSERROR 75 Временный сбой, повторите операцию');
INSERT INTO public.code_job_exit(code, brief)	VALUES (76, 'SYSERROR 76 Ошибка протокола удаленного узла');
INSERT INTO public.code_job_exit(code, brief)	VALUES (77, 'SYSERROR 77 Недостаточно прав для выполения операции');
INSERT INTO public.code_job_exit(code, brief)	VALUES (78, 'SYSERROR 78 Ошибка конфигурации');
INSERT INTO public.code_job_exit(code, brief)	VALUES (126, 'SYSERROR 126 Команда не может быть выполнена');
INSERT INTO public.code_job_exit(code, brief)	VALUES (127, 'SYSERROR 127 Команда не найдена');
INSERT INTO public.code_job_exit(code, brief)	VALUES (128, 'SYSERROR 128 Неверный аргумент завершения');


drop table if exists  public.code_band_combination;
CREATE TABLE IF NOT EXISTS public.code_band_combination
(
    code smallint NOT NULL,
	brief varchar(50),
	note varchar(256),
    CONSTRAINT code_band_combination_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_band_combination(code, brief)	VALUES (0, 'PAN');
INSERT INTO public.code_band_combination(code, brief)	VALUES (1, 'MS');
INSERT INTO public.code_band_combination(code, brief)	VALUES (2, 'PMS');
INSERT INTO public.code_band_combination(code, brief)	VALUES (3, 'BUNDLE');
INSERT INTO public.code_band_combination(code, brief)	VALUES (255, 'UNDEF');

drop table if exists  public.code_level;
CREATE TABLE IF NOT EXISTS public.code_level
(
    code smallint NOT NULL,
	brief varchar(50),
	note varchar(256),
    CONSTRAINT code_level_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_level(code, brief)	VALUES (0, 'RAW');
INSERT INTO public.code_level(code, brief)	VALUES (1, 'L0');
INSERT INTO public.code_level(code, brief)	VALUES (2, 'L1');
INSERT INTO public.code_level(code, brief)	VALUES (3, 'L2');
INSERT INTO public.code_level(code, brief)	VALUES (4, 'L3M');
INSERT INTO public.code_level(code, brief)	VALUES (5, 'L3C');
INSERT INTO public.code_level(code, brief)	VALUES (255, 'UNDEF');


drop table if exists  public.code_priority;
CREATE TABLE IF NOT EXISTS public.code_priority
(
    code smallint NOT NULL,
	brief varchar(50),
	note varchar(256),
    CONSTRAINT code_priority_pk PRIMARY KEY (code)
)
TABLESPACE pg_default;
INSERT INTO public.code_priority(code, brief)	VALUES (0, 'LOW');
INSERT INTO public.code_priority(code, brief)	VALUES (1, 'NORMAL');
INSERT INTO public.code_priority(code, brief)	VALUES (2, 'HIGH');
INSERT INTO public.code_priority(code, brief)	VALUES (255, 'UNDEF');


alter table public."MsgPair" add "in_params_isPortal" small
alter table public."MsgPair" add "in_params_bandAutoAssign" SMALLINT