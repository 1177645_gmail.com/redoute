##Клонирование проекта
git clone 
git config --global credential.helper store
С этого момента Git будет записывать учетные данные в файл ~/.git-credentials для каждого контекста URL при первом обращении.

##Установка gdal2tiles.py Centos

1. sudo yum makecache
2. sudo yum -y install gdal-python

##Установка gdal2tiles.py Ubuntu22

1. sudo apt install gdal-bin
2. 


##Порезка на тайлы 8битных тиф файлов

1. преобразование в 8бит
    gdal_translate -of VRT -ot Byte -scale <tiff file> <vrt file>
2. порезка на тайлы
    gdal2tiles.py -x --xyz --processes 1 --profile=mercator -z 0-17  --tilesize 256 <vrt file> <out folder>


##xml xpath
https://extendsclass.com/xpath-tester.html

Работа с растрами
https://habr.com/ru/articles/841886/

COG format
https://www.gillanscience.com/cloud-native-geospatial/cog/