import { google } from 'googleapis'

export class GoogleApi {

  sheets = google.sheets('v4');
  SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

  keyFilename = './src/makersheet-key.json'

  async getAuthToken() {
    const auth = new google.auth.GoogleAuth({
      keyFilename:  this.keyFilename,
      scopes:       this.SCOPES
    });
    const authClient = await auth.getClient();
    return authClient
  }

  async getSpreadSheet({spreadsheetId, auth}) {
    const res = await this.sheets.spreadsheets.get({
      spreadsheetId,
      auth,
    });
    return res;
  }

  async getSpreadSheetValues({spreadsheetId, auth, sheetName}) {
    const res = await this.sheets.spreadsheets.values.get({
      spreadsheetId,
      auth,
      range: sheetName
    });
    return res;
  }

  async test() {
    const spreadsheetId = '1e6APRxSOqUUUbMeF0qsuR5lL2w_d8Kxxx0uMsq9awJk'
    const sheetName =     '11.5_5_adygea_kv_pms_e9545d5e'

    try {
      const auth = await this.getAuthToken();
      let response = await this.getSpreadSheet({
        spreadsheetId,
        auth
      })
      console.log('output for getSpreadSheet', JSON.stringify(response.data, null, 2));

      response = await this.getSpreadSheetValues({
        spreadsheetId,
        sheetName,
        auth
      })
      console.log('output for getSpreadSheetValues', JSON.stringify(response.data, null, 2));      
    } catch(error) {
      console.log(error.message, error.stack);
    }
  }
}

let g = new GoogleApi()
g.test()
