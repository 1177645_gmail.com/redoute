
export let allCode = {}

allCode['sceneStatus'] = {
  '0':    'GOOD',
  '1':    'BAD_STAT',
  '2':    'BAD_RFM',
  '3':    'BAD_DATA_LOSSES',
  '4':    'BAD_DATA_NOISE',
  '5':    'BAD_STITCHING',
  '6':    'BAD_GAIN_CHANGE',
  '7':    'BAD_USEFUL_AREA',
  '255':  'UNDEF',  
}  

allCode['routeStatus'] = {
  '0':    'GOOD',
  '1':    'BAD',
  '255':  'UNDEF',  
}  

allCode['maskType'] = {
  '0':    'CLOUD',
  '1':    'SHADOWS',
  '2':    'WATER',
  '3':    'SNOW',
  '6':    'DEFECTS',
  '7':    'DATA_LOSSES',
  '255':  'UNDEF',
}

allCode['in_priority'] = {
  '0':    'LOW',
  '1':    'NORMAL',
  '2':    'HIGH',
  '255':  'UNDEF',  
}  

allCode['in_type'] = {
  '0':    'TASK',
  '1':    'STATUS_CHECK',
  '2':    'RESTART',
  '3':    'DELETE',
  '4':    'CHANGE',
  '255':  'UNDEF',  
}  

allCode['in_params_rasterType'] = {
  '0':    'NATIVE',
  '1':    'ALIEN',
  '255':  'UNDEF',  
}  

allCode['in_params_level'] = {
  '0':    'RAW',
  '1':    'L0',
  '2':    'L1',
  '3':    'L2',
  '4':    'L3M',
  '5':    'L3C',
  '255':  'UNDEF',  
}  

allCode['in_params_bandCombination'] = {
  '0':    'PAN',
  '1':    'MS',
  '2':    'PMS',
  '3':    'BUNDLE',
  '255':  'UNDEF',  
}  


allCode['out_taskStatus'] = {
  '0':    'NEW_TASK',
  '1':    'NOT_FOUND',
  '2':    'DUPLICATE',
  '3':    'PENDING',
  '4':    'PROCESSING',
  '5':    'DONE',
  '6':    'DELETED',
  '7':    'KILLED',
  '8':    'STOPPED',
  '255':  'UNDEF',  
}  

allCode['out_jobStatus'] = {
  '0':    'SUCCESS',
  '1':    'PARTIAL_SUCCESS',
  '2':    'ERROR',
  '255':  'UNDEF',  
}  

allCode['out_type'] = {
  '0':    'TASK_STATUS',
  '255':  'UNDEF',  
}  

allCode['in_params_rasterFormat'] = {
  '0':    'GTF',
  '1':    'WMS',
  '2':    'JPG',
  '3':    'JP2',
  '4':    'JTF',
  '5':    'RAW',
  '6':    'COG',
  '255':  'UNDEF',  
}  

allCode['in_params_compression'] = {
  '0':    'DEFLATE',
  '1':    'LZW',
  '2':    'LZMA',
  '3':    'JPEGLS',
  '4':    'ZSTD',
  '5':    'LZ4',
  '6':    'CLZ',
  '7':    'SNAPPY',
  '8':    'JP2HT',
  '9':    'JP2',
  '10':    'NONE',
  '11':    'MIXED',
  '255':  'UNDEF',  
}  

