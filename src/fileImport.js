import fs from 'fs'
import { exec, execSync } from 'child_process'
import readdirp from 'readdirp'
import { workerData } from 'worker_threads'
import path from 'path'

async function makeMessage(data) {
  if (!data) return
  console.log('Worker - makeMessage')

  let { uid, config, message } = data 
  if (!uid) makeTask(data)
  else makeCommand(message, uid, config)
}

async function makeTask(data) {
  let ret = []
  let { level, bandCombination, input, polygon, taskPrefix, folder, params, priority, config, message } = data 

  if (folder) {
    if (!fs.existsSync(folder)) throw 'Не найден ' + folder
    ret = await makeProductFromFolder(level, bandCombination, folder, params, priority, config, taskPrefix)
  }
  else {
    if (input.length == 0) return ret

    let bcExt  = []
    if (bandCombination == 'PAN_MS_PMS') {
      bcExt.push('PAN')
      bcExt.push('MS')
      bcExt.push('PMS')
    }
    else
      bcExt.push(bandCombination)

    let arData = []
    let inputId = null
    let taskName =  uuid()
    // если мозаика|композит то делаем одно задание
    if (level && level.includes('L3') )  {
        arData.push(input)
    }
    else arData = input
    // все команды сначала пишем в файл
    let forpostFile = uuid()
    for (let bc of bcExt) {
      for (let data of arData)  {
        let cmd = []
        taskName = uuid()
        if (taskPrefix) taskName = taskPrefix + '_' + bc.toLowerCase() + '_' + uuid()
        cmd.push(makeCmdTask(forpostFile, taskName, level, bc, polygon, makeId(data), params, priority, config, message))
        // запись в файл 
        start(cmd)
        ret.push({taskName: taskName})
      }
    }
    // а теперь передаем все в форпост и выполняем
    start(makeCmdForpost(forpostFile, config))
  }
  return ret
 }


async function makeProductFromFolder(level, bc, folder, params, priority, config, taskPrefix)   {
  if (!folder) return
  console.log('Импорт данных из папки ' + folder)
  let ret = []

  let allKa = [ 'RP1', 'RP2', 'RP3', 'RP4', 'RP5',  
                'KV1', 'KVI', 'KV3', 'KV4', 'KV5', 'KV6', 
                'M20', 'M22', 'M23', 'M24',
                'AT1',
                '004'
              ]

  let arPath = []
  let ar = await readdirp.promise(folder, { type: 'directories', depth: 0, directoryFilter: [ '*' ] })
  for (let fpath of ar)
    arPath.push(path.join(folder,fpath.path))
  let cmd = []
  // берем файлы маршрутов в каждой папке и все команды для форпоста пишем в один файл
  let forpostFile = uuid()
  let allInOne = true
  for (let dir of arPath)  {
    console.log(dir)
    let f80x = await readdirp.promise(dir,{ type: 'files', depth: 0, fileFilter: [ '80*.zip', '80*.xml' ] })
    let md =  await readdirp.promise(dir,{ type: 'files', depth: 0, fileFilter: [ '*.L0.MD.xml' ] })
    
    if (f80x.length > 0) {
      // если прием с антенны, то данные raw в 2-х вложенных папках
      let raw = await readdirp.promise(dir,{ type: 'files', depth: 1, fileFilter: [ '*raw' ] })
      if (raw.length == 0) {
        console.log('skip ' + dir)
        continue
      }
    }

    let k = dir.lastIndexOf('/')
    let ka = dir.substring(k+1,k+4)
    if (!allKa.includes(ka)) continue;
    if (ka == '004') {
      ka = 'RP' + dir.substring(k+4,k+5)
    }

    let inputLevel = 'L0'
    let productFormat = 'ALIEN'
    if (md.length > 0) productFormat = 'NATIVE'
    else if (f80x.length > 0) { 
      inputLevel = 'RAW'
      productFormat = ''
    }

    let taskUid = uuid()
    if (taskPrefix) taskUid = taskPrefix + "_" + taskUid
    let c = makeCmdProduct(level, bc, forpostFile, taskUid, ka, dir, inputLevel, productFormat, params, priority, config)
    cmd.push(c)
    ret.push({taskName: taskUid, folder: dir})
  }

  if (cmd.length == 0) {
    console.log("Не найдены маршруты." + 
    "\nКаждый маршрут/сброс должен начинаться с KV<номер аппарата> RP<номер аппарата> M2...0234 + AT<номер аппарата>" +
    "\nПередавать нужно корневую папку, в которой маршруты на первом уровне")
    return
  }
  // выполняем запись в файл
  start(cmd);
  // а теперь передаем все в форпост
  start(makeCmdForpost(forpostFile, config))
  return ret
}

function makeCmdProduct(level, bc, forpostFile, uid, ka, path, inputLevel, productFormat, tamarinParam, priority, config) {
  if (!path) return ''
  let device = ''  
  if (inputLevel != 'RAW') {
    if      (path.includes('GEOTON'))     device = 'GEOTON'
    else if (path.includes('KANOPUS'))    device = 'KANOPUS'
    else if (path.includes('KSHMSA-SR'))  device = 'KSHMSA-SR'
    else if (path.includes('KSHMSA-VR'))  device = 'KSHMSA-VR'
    else if (path.includes('KSHMSA-VR1')) device = 'KSHMSA-VR1'
    else if (path.includes('KSHMSA-VR2')) device = 'KSHMSA-VR2'
    else if (path.includes('KMSS'))       device = 'KMSS'
    else if (path.includes('MSU-MR'))     device = 'MSU-MR'
    else if (path.includes('AURORA1'))    device = 'AURORA1'
    else if (path.includes('AURORA2'))    device = 'AURORA2'

    if (!device) {
      if      (ka.startsWith('KV')) device = 'KANOPUS'
      else if (ka.startsWith('RP')) device = 'GEOTON'
      else if (ka.startsWith('M2')) device = 'KMSS'
      else if (ka.startsWith('AT')) device = 'AURORA1'
    }
  }

  if (device) device = '--device ' + device 

  let mbc = ''
  if (bc) mbc = '--band-combination ' + bc
  let mLevel = ''
  if (level) mLevel = '--level ' + level

  let priorityParam = ''
  if (priority) priorityParam += '--priority ' + priority 

  if (productFormat) productFormat = ' --product-format ' + productFormat
  let { ssh } = config
  return  ssh + ' \'' + 
          ' cd ' + config.tmpDir + 
          ' && ' +
          ' echo -p --uid ' + uid + 
          ' -msg TASK' +
          ' ' + mLevel + 
          ' ' + mbc +
          ' ' + tamarinParam +
          ' --satellite ' + ka + 
          ' --input-level ' + inputLevel + 
          ' ' + productFormat + 
          ' '+ device + 
          ' --input ' + path + 
          ' ' + priorityParam +
          ' >> ' + forpostFile + 
          ' \''
}

export function makeCmdForpost(forpostFile, config) {
  let { ssh } = config
  let forpost = config.forpostCmd
  return ssh + ' \'' + ' cd ' + config.tmpDir + ' && ' + forpost + ' -i ' + forpostFile + '\''
}

export function makeCmd(forpostFile, uid, cmd, config) {
  if (!uid) return
  let { ssh } = config
  return  ssh + ' \'' + 
          ' cd ' + config.tmpDir + 
          ' && ' +
          ' echo -p --uid ' + uid + 
          ' -msg ' + cmd +
          ' >> ' + forpostFile + 
          ' \''
}

function makeCmdTask(forpostFile, uid, level, bc, coords, id, params, priority, config, message) {
  if (!id || id.length == 0) throw 'не заданы id продуктов'

  // все параметры определяем по первому id в строке 
  let input = id.split(',')[0]
  // берем все после подчеркивания перед датой
  let device = null
  let ka = null
  let dev =input.match(/id:(\w{3}).*_(.+)_20\d{6}_.*\.L0/)
  if (dev) {
    ka = dev[1]
    device = dev[2]
    if (device.startsWith('KMSS')) device = 'KMSS'
  }
  if (!device || !ka) throw 'Не удалось определить device|ka :' + input

  let mosaicName = ''
  if (level)
    if(level.includes('L3')) mosaicName = ' --mosaic-name ' + uid;

  let cropParam = ''
  let crop = ''

  if (coords && coords.length > 0)  {
    let fullCrop = {
      type:   "FeatureCollection",
      name:   "outline",  
      crs:  {
        type: "name",
        properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" }
      },
      features: [ {
          type: "Feature", 
          properties: { },
          geometry: {
            type: "Polygon",
            coordinates: [ coords ]
          }
        }
      ]
    }
    fs.writeFileSync(config.tmpDir + '/' + uid + '.json', JSON.stringify(fullCrop));
    cropParam = ' --crop-by-shape ' + uid + '.json'
  }

  let mbc = ''
  if (bc) mbc = ' --band-combination ' + bc
  let mLevel = ''
  if (level) mLevel = ' --level ' + level
  let mParams = ''
  if (params) mParams = params

  let priorityParam = ''
  if (priority) priorityParam += ' --priority ' + priority 

  let { ssh } = config
  return  ssh + ' \'' + 
          ' cd ' + config.tmpDir  + 
          ' && ' +
          ' echo -p --uid ' + uid + 
          cropParam + 
          ' -msg ' + message + ' ' +
          mLevel +
          mbc + 
          mosaicName + 
          ' --satellite ' + ka + 
          priorityParam + 
          ' ' + mParams + 
          ' --input-level L0 --product-format NATIVE --device ' + device + 
          ' --input ' + id + 
          ' >> ' + forpostFile + 
          ' \''
}

function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  }).substring(0,8);
}

export function start(cmd, sync) {
  if (sync == undefined) sync = true
  let started = 0;
  if (cmd instanceof Array) {
    for (let c of cmd) {
      if (c) { 
        console.log(c)
        if (sync)        execSync(c, { stdio: 'ignore' } )
        else         exec(c, { stdio: 'ignore' } )
        started++
      }
    }
  }
  else {
    console.log(cmd)
    if (sync)        execSync(cmd, { stdio: 'ignore' } )
    else         exec(cmd, { stdio: 'ignore' } )
    started++
  }

  let report = 'Started ' + started
  return report  
  
}

export async function makeCommand(name, taskUid, config) {

  let forpostFile = uuid();
  let cmd = []
  for (let uid of taskUid) {
    let c = makeCmd(forpostFile, uid, name, config)
    cmd.push(c)
  }
  start(cmd)
  start(makeCmdForpost(forpostFile, config))
}

function makeId(routes)  {
  let ret = ''
  if (Array.isArray(routes)) {
    for (let route of routes) {
        if (!route) continue
        if (ret) ret += ','
          ret += 'id:' + route
    }
  }
  else 
    ret += 'id:' + routes
  return ret;
}

makeMessage(workerData)