import http from 'http'
import pg from 'pg'
import fs, { readFile }  from 'fs'
import xpath from 'xpath'
import minimist from 'minimist'
import xmldom from 'xmldom'
import readdirp from 'readdirp'
import path from 'path'
import { exec, execSync } from 'child_process'
import shapefile from 'shapefile'
import * as turf from '@turf/turf'
import images from 'images'
import GeoTIFF, { fromFile } from 'geotiff'
import polygonClipping from 'polygon-clipping'
import * as dconfig from '../config.js'
import { Tiler } from './tiler.js'
import { allCode } from './code.js'
import { start, makeCommand  } from './fileImport.js'
import { Worker } from 'worker_threads'
import { readTile, convertArray2Png } from './cog.js'

import parquet from '@dsnp/parquetjs'

let allConfig = {
  dev1:       dconfig.dev1,
  dev2:       dconfig.dev2,
  dev3:       dconfig.dev3,
  dev4:       dconfig.dev4,
  dev5:       dconfig.dev5,
  dev6:       dconfig.dev6,
  dev7:       dconfig.dev7,
  dev8:       dconfig.dev8,
  dev9:       dconfig.dev9,

  dev1x:      dconfig.dev1x,
  dev2x:      dconfig.dev2x,

  arc:        dconfig.arc, 

  ffd_old:    dconfig.ffd_old, 
  production: dconfig.production, 

  ffd:     dconfig.ffd,
  dig:     dconfig.dig,
}

let config = null  
let dbWork = null
let dbL0 = null
let dbL2 = null

let tiler = new Tiler()

function  initConfig(configJson) {
  if (!configJson) {
    let configFile = 'config.json'
    if (fs.existsSync(configFile)) {
      configJson = fs.readFileSync(configFile)
      configJson = JSON.parse(configJson)
    }
    else 
      throw "файл 'config.json' was not found"
  }
  if (configJson.name) {
    console.log(configJson.name)
    config = allConfig[configJson.name]
    if (!config) throw 'В списке возможных конфигураций не найдено ' + configJson.name
    dbWork = null
    dbL0 = null
    dbL2 = null

    if (!fs.existsSync(config.rootConfig)) throw config.rootConfig + ' не найден. При запуске с локальной машины не смонтирована папка '
  }
}

async function  initDb() {
  try {
    if (dbWork == null) {
      dbWork = new pg.Client(config.db)
      await dbWork.connect()
    }
    if (dbL0 == null) {
      dbL0 = new pg.Client(config.dbL0)
      await dbL0.connect()
    }
    if (dbL2 == null) {
      dbL2 = new pg.Client(config.dbL2)
      await dbL2.connect()
    }
  }
  catch (e) {
    console.log(e)
  }
}

async function requestListener(req, res) {
  let result = []

    //console.log(req.url)
    if (!req.url) return

    let jsonData =  {}
    if (req.method == 'POST') {
      let postData =  ''
      req.on('data',(chunk) => { postData += chunk; })
      req.on('end',() => {  
        let jsonData = {}
        if (postData) jsonData = JSON.parse(postData)
        return processRequest(req, res, jsonData)
      })
      return
    }
    processRequest(req, res, {}) 
  }

async function processRequest(req, res, jsonData) {
  let result = ''
  try 
  {
    const url = new URL("http:/localhost" + req.url)
    let pathName = url.pathname.substring(1)
    let taskId = url.searchParams.get("task_id")
    let jobId = url.searchParams.get("job_id")
    let productId = url.searchParams.get("product_id")
    let lastUpdate = url.searchParams.get("lastUpdate")
    let name = url.searchParams.get("name")
  
    await initDb()

    res.setHeader("Access-Control-Allow-Origin", "*")
    switch (pathName) {
      case "task": 
        result = await runTask(jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "uid": 
        result = await getTaskUid(taskId);
        if (!result) result = 'Не найден uid по ' + taskId
        let obj = {uid: result};
        result = JSON.stringify(obj)
        res.setHeader("Content-Type", "application/json")
        break
      case "job": 
        result = await runJob(taskId, jobId, jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "approved": 
        result = await runApproved(taskId);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "log": 
        result = await runLog(taskId,jobId,name);
        res.setHeader("Content-Type", "text/plain")
        break
      case "file": 
        result = await runFile(name);
        res.setHeader("Content-Type", "text/plain")
        break
      case "config": 
        result = await runConfig(jsonData);
        res.setHeader("Content-Type", "text/plain")
        break
      case "stand": 
        result = await runStand(jsonData);
        res.setHeader("Content-Type", "text/plain")
        break
      case "image": 
        result = await runImage(name);
        res.setHeader("Content-Type", "image/jpg")
        break
      case "images": 
        result = await runImages(taskId, jobId, productId, jsonData);
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "tile": 
        result = await runTile(req.url)
        //res.setHeader("Content-Type", "image/png")
        break
      case "tiff": 
        result = runTiff(req.url)
        res.setHeader("Content-Type", "image/tiff")
        break
      case "restart": 
        result = await runCmd('RESTART',jsonData);
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "stop": 
        result = await runCmd('STOP',jsonData);
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "delete": 
        result = await runCmd('DELETE',jsonData);
        res.setHeader("Content-Type", "text/plain")
        result = JSON.stringify(result)
        break
      case "deleteItem": 
        result = await runDeleteItem(jsonData);
        res.setHeader("Content-Type", "text/plain")
        result = JSON.stringify(result)
        break
      case "refine": 
        result = await runRefine(taskId)
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "crop": 
        result = await runCrop(taskId, jobId, productId, jsonData)
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "mask": 
        result = await runMask(taskId, jobId, productId, jsonData)
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "border": 
        result = await runBorder(productId)
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        console.log(result)
        break
      case "geoborder": 
        result = await runGeoborder();
        res.setHeader("Content-Type", "text/plain")
        break
      case "updateL2": 
        result = await runUpdateL2();
        res.setHeader("Content-Type", "text/plain")
        break
      case "ql": 
        result = await runQL();
        res.setHeader("Content-Type", "text/plain")
        break
      case "find": 
        result = await runFind(jsonData);
        res.setHeader("Content-Type", "application/json")
        result = JSON.stringify(result)
        break
      case "catalog": 
        result = await runCatalog(taskId, lastUpdate);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "report": 
        result = await runReport(jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "files": 
        result = await runFiles(jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "kit": 
        result = await runKit(jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "make": 
        result = await runMake(jsonData);
        res.setHeader("Content-Type", "text/plain")
        break
      case "fromReport": 
        result = await runFromReport(jsonData);
        res.setHeader("Content-Type", "text/plain")
        break
    case "loadProductList": 
        result = await runLoadProductList(name)
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
    case "loadKit": 
        result = await runLoadKit(name)
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
    case "loadQuery": 
        result = await runLoadQuery(name)
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "saveProduct": 
        result = await runSaveProduct(jsonData)
        res.setHeader("Content-Type", "text/plain")
        break
      case "saveLink": 
        result = await runSaveLink(jsonData)
        res.setHeader("Content-Type", "text/plain")
        break
      case "saveKit": 
        result = await runSaveKit(jsonData)
        res.setHeader("Content-Type", "text/plain")
        break
      case "polygon": 
        result = await runPolygon(jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "quality": 
        result = await runQuality(taskId, jobId, productId, jsonData);
        result = JSON.stringify(result)
        res.setHeader("Content-Type", "application/json")
        break
      case "taskInfo": 
        result = await getTaskInfo(taskId);
        result = JSON.stringify(obj)
        res.setHeader("Content-Type", "application/json")
        break

      default:
        throw "Обработка не реализована для url " + req.url
    }
    res.writeHead(200)
    res.end(result,'utf8')
  }
  catch(e) {
    result = e
    console.log(result)
    if (e.stack) console.log(e.stack)
    res.setHeader("Content-Type", "text/plain")
    res.writeHead(500)
    res.end(result.toString(),'utf8')
  }
}

async function runFromReport(jsonData) {
  let ret = ''
  let { path, needKey, type } = jsonData 
  
  let data = fs.readFileSync(path)
  let ar = data.toString().split('\n')
  let result = []
  for (let line of ar) {
    let arKeyValue = line.toString().split('\t')
    let r = {}
    for (let i=0; i<arKeyValue.length; i+=2) {
      let key = arKeyValue[i]
      if (key == 'shortName') {
        r.shortName = arKeyValue[i+1]
      }
      if (key == needKey) {
        r.need = arKeyValue[i+1]
        if (type == 'text') r.need = '\'' + r.need + '\''
        if (r.need != 'no_value')
          result.push(r)
      }
    }
  }
  for (let rv of result) {
    let sqlUpdate = 'update "CatalogItem" set ' + needKey + ' = ' + rv.need + ' where "productId" like \'' + rv.shortName + '%\''
    console.log(sqlUpdate);
    dbL0.query(sqlUpdate)
  }
  return 'started ' + ar.length
 }

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

function printGeometry (coords) {
  if (!coords || coords.length == 0) return ''
  var res = '';
  if (Array.isArray(coords)) {
      res = '( ';
      for (var i = 0, l = coords.length; i < l; i++) {
          if (i > 0) {
              res += ', ';
          }
          res += printGeometry(coords[i]);
      }
      res += ' )';
  } else if (typeof coords == 'number') {
      res = coords.toPrecision(6);
  } else if (coords.toString) {
      res = coords.toString();
  }

  return res;
}

async function runFind(jsonData) {
  let polygon = printGeometry(jsonData.polygon)

  // проверяем полигон - должен быть по ширине меньше 180
  let sqlP = "SELECT width(box (polygon'" + polygon + "')) as width"
  sqlP+= ' from "PM" limit 1'
  const rowsP  = await dbL2.query(sqlP)
  if (rowsP.rows[0].width > 180) throw 'Контур попадает на антимеридиан или больше 180'

  let operation = '&&'
  if (jsonData.operation == 'intersection') operation = '&&'
  else if (jsonData.operation == 'contain') operation = '@>'

  let where = ''
  if (polygon) {
    where = ' where "geoLocation_geoBorder" ' + operation + '\'' + polygon + '\''
    // исключим сцены, которые попадают на антимеридиан
    where += ' and width(box("geoLocation_geoBorder")) < 180'
  }

  let sql =  "SELECT distinct concat(substring(\"productId\" from '(.*)\.L2'),'.L0') as product, value_before_filtered_gcp_stat_mean as \"refineBefore\", value_after_regular_gcp_stat_mean as \"refineAfter\""  
  + ' from "PM" t1 left join "PM_qa_fine_estimationMap" t2 on t1."productId" = t2.object_id'
  + where
  + " order by concat(substring(\"productId\" from '(.*)\.L2'),'.L0') asc"
  console.log(sql)
  const { rows } = await dbL2.query(sql)
/*
  Старый поиск через дополнительное поле в каталоге л0
  if (polygon) where = ' where t1.geoborder ' + operation + '\'' + polygon + '\''
  let sql =   'SELECT "productId" as product, t1.rfm_before_mean as "refineBefore", t1.rfm_after_mean as "refineAfter", t1.rfm_version as "refineVersion", t1."softwareVersion" as "archivingVersion", to_char(timezone(\'utc\',t1."archivingTime_ptime"), \'YYYY-MM-DD HH24:MI:SS\') as "archivingTime",  t2.brief as input' 
            + ' from "CatalogItem" t1 inner join code_input t2 on t1."inputLevel" = t2.code'
            + where
            + ' order by t1."productId" asc'
  console.log(sql)
  const { rows } = await dbL0.query(sql)
*/            
  return rows
}

async function runTask(jsonData) {
  let { product_id, lastUpdate, uid, portal } = jsonData
  // имя задания берем из первой операции 
  let qLast = ''
  let qPortal = ''
  let qUid = ''
  let joinProduct = ''
  let whereProduct = ''
  if (uid) {
    qUid = ' and in_uid = \'' + uid + '\' ' 
  }
  else {
    if (product_id) {
      joinProduct = ' left join "MsgPair_out_jobList" t10 on t1.id=t10.object_id '
      let upperProductId = product_id.toUpperCase()
      whereProduct = ' and (array_to_string(t10."value_output",\';\') like \'%' + upperProductId + '%\' or array_to_string(t10."value_input",\';\') like \'%' + upperProductId + '%\')'
    }
    if (lastUpdate) {
      qLast = ' and (t1.out_created_ptime > \'' +     lastUpdate + '\''
              + ' or t1.out_started_ptime > \'' +  lastUpdate + '\''
              + ' or t1.out_ended_ptime > \'' +    lastUpdate + '\''
              + ')' 
    }
    if (portal == true) {
      qPortal = ' and t1."in_params_isPortal" = true '
    }

  }
  let sql = 'SELECT distinct t1.out_created_ptime, id as id_task, in_uid as task_uid' + 
  ', to_char(timezone(\'utc\',t1.out_created_ptime), \'YYYY-MM-DD HH24:MI:SS\') as create' + 
  ', to_char(timezone(\'utc\',t1.out_started_ptime), \'YYYY-MM-DD HH24:MI:SS\') as start' + 
  ', to_char(timezone(\'utc\',t1.out_ended_ptime),   \'YYYY-MM-DD HH24:MI:SS\') as finish' + 
  ', t2.brief as task_status' + 
  ', t3.brief as job_status' +
  ', (date_trunc(\'seconds\',t1.out_ended_ptime)-date_trunc(\'seconds\',t1.out_started_ptime))::text duration' +
  ', concat(t5.brief,\' (\',t6.brief,\')\') as level ' + 
  ' from "MsgPair" t1' +
  ' left join  code_task_status t2 on t1."out_taskStatus"=t2.code' + 
  ' left join code_job_status t3 on t1."out_jobStatus"=t3.code' + 
  ' left join "code_level" t5 on t1.in_params_level=t5.code' + 
  ' left join "code_band_combination" t6 on t1."in_params_bandCombination"=t6.code' + 
  joinProduct + 
  ' where true ' + qLast + qPortal + qUid + whereProduct + 
  ' order by t1.out_created_ptime desc';
  //console.log(sql)      
  const { rows } = await dbWork.query(sql)
  return rows
}

async function runCatalog(taskId, lastUpdate) {
  let sql = 'SELECT t1."productId" as product, t1.rfm_before_mean as "refineBefore", t1.rfm_after_mean as "refineAfter", t1.rfm_version as "refineVersion", t1."softwareVersion" as "archivingVersion", to_char(timezone(\'utc\',t1."archivingTime_ptime"), \'YYYY-MM-DD HH24:MI:SS\') as "archivingTime",  t2.brief as input ' +
  ' from "CatalogItem" t1 left join code_input t2 on t1."inputLevel" = t2.code' +
  ' order by t1."productId" asc';
  console.log(sql)      
  const { rows } = await dbL0.query(sql)
  return rows
}

async function runReport(jsonData) {
  let { name, action } = jsonData
  let path = config.path['report']
  // если имя не задано - вернем все отчеты
  if (action == 'list') {
    let arFile = []
    let arPath = await readdirpPromiseArray(path,{ type: 'files' })
    for (let p of arPath)  
      arFile.push(p.path)
    return arFile
  }
  path += '/' + name
  console.log(path)      
  if (!fs.existsSync(path)) throw 'Не найден ' + path
  let sql = fs.readFileSync(path, {encoding: 'UTF-8'})
  console.log(sql)      
  let dbUse = dbL2
  if (path.includes('report/l0/')) dbUse = dbL0
  else if (path.includes('report/work/')) dbUse = dbWork
  else if (path.includes('report/l2/')) dbUse = dbL2
  const { rows } = await dbUse.query(sql)
  replaceCode(rows)
  return rows
}

// Возвращаем на всех уровнях все файлы из заданной папки
async function runFiles(jsonData) {
  let { name } = jsonData
  let path = config.path[name]
  let arFile = []
  let arPath = await readdirpPromiseArray(path,{ type: 'files' })
  for (let p of arPath)  
    arFile.push(p.path)
  return arFile
}

async function runKit(jsonData) {
  let { name } = jsonData
  let path = config.kitPath
  // если имя не задано - вернем все отчеты
  if (!name) {
    let arFile = []
    let arPath = await readdirpPromiseArray(path,{ type: 'files' })
    for (let p of arPath)  
      arFile.push(p.path)
    return arFile
  }
  path += '/' + name
  console.log(path)      
  if (!fs.existsSync(path)) throw 'Не найден ' + path
  let sql = fs.readFileSync(path, {encoding: 'UTF-8'})
  console.log(sql)      
  let dbUse = dbL2
  if (path.includes('report/l0/')) dbUse = dbL0
  else if (path.includes('report/work/')) dbUse = dbWork
  const { rows } = await dbUse.query(sql)
  if (sql.includes('_code'))  replaceCode(rows)
  return rows
}

function replaceCode(rows) {
  // сделаем замену полей ..._code на символьные значения 
  for (let row of rows) {
    for (let field in row) {
      if (field.includes('_code')) {
        let fieldNoCode = field.replace('_code','')
        let code = row[field]
        let br = brief(fieldNoCode,code)
        if (br) { row[field] = br; }
      }
    }
  }
}

async function runLoadProductList(name) {
  // имя задания берем из первой операции 
  if (!name) return []
  let path = config.tmpDir + '/' + name
  if (!fs.existsSync(path)) throw 'Не найден ' + path
  let data = fs.readFileSync(path)
  let ar = data.toString().split('\n')
  let arProduct = []
  let sqlProduct = ''
  for (let mix of ar) {
    let m = mix.split('\t')
    if (!m[0]) continue
    arProduct.push(m[0])
    if (sqlProduct) sqlProduct += ','
    sqlProduct += "'" + m[0] + "'"
  }
  console.log(arProduct)

  let sql =   'SELECT "productId" as product, t1.rfm_before_mean as "refineBefore", t1.rfm_after_mean as "refineAfter", t1.rfm_version as "refineVersion", t1."softwareVersion" as "archivingVersion", to_char(timezone(\'utc\',t1."archivingTime_ptime"), \'YYYY-MM-DD HH24:MI:SS\') as "archivingTime",  t2.brief as input' 
            + ' from "CatalogItem" t1 inner join code_input t2 on t1."inputLevel" = t2.code'
            + ' where "productId" in (' + sqlProduct + ')' 
            + ' order by t1."productId" asc'
  console.log(sql)      
  const { rows } = await dbL0.query(sql)
  return rows
}

async function runSaveProduct(jsonData) { 
  let ret = ''
  let { input, name, action } = jsonData 
  let data = ''
  let path = config.tmpDir + '/' + name
  
  if (action == 'list') {
    for (let product of input) {
      if (data) data += '\n'
      data += product.productId + '\t' + product.meanBefore + '\t' + product.meanAfter
    }
    fs.writeFileSync(path, data)
    return 'Записано маршрутов: ' + input.length
  }
  else if (action == 'link') {
    // удаляем и создаем папку
    if (!fs.existsSync(path)) 
      fs.mkdirSync(path)
    let okCount = 0
    for (let product of input) {
      if (data) data += '\n'
      let d =product.productId.match(/.*_(20\d{2})(\d{2})(\d{2})_.*/)
      if (d) {
        let cmdLink = config.ssh 
          + ' "'
          + 'ln -s ' + config.archiveL0 + '/' + d[1] + '/' + d[2] + '/' + d[3] + '/' + product.productId + ' ' + path 
          + ' "'
        console.log(cmdLink)
        try {
          execSync(cmdLink)
          okCount++
        }
        catch(e) {
          console.log(e)
        }
      }
    }
    return 'Сформировано ссылок: ' + okCount
  }
}

async function runSaveLink(jsonData) { 
  let ret = ''
  let { input, name, action } = jsonData 
  let data = ''
  let path = name
  if (!fs.existsSync(path)) throw 'Не найдена папка ' + path
  
  let okCount = 0
  for (let product of input) {
    let prodRoot = config.archiveL0
    let productId = product.productId
    if (productId.includes('.SCN')) prodRoot = config.archiveL2
    let d = productId.match(/.*_(20\d{2})(\d{2})(\d{2})_.*/)
    if (d) {
      let cmdLink = config.ssh 
        + ' "'
        + 'ln -s ' + prodRoot + '/' + d[1] + '/' + d[2] + '/' + d[3] + '/' + product.productId + ' ' + path 
        + ' "'
      console.log(cmdLink)
      execSync(cmdLink)
      okCount++
    }
  }
  return 'Сформировано ссылок: ' + okCount
}

async function runSaveKit(jsonData) { 
  let ret = ''
  let { input, name, action } = jsonData 
  let data = ''
  let path = config.path['kit'] + '/' + name
  
  for (let product of input) {
    if (data) data += '\n'
    data += product.productId
  }
  fs.writeFileSync(path, data)
  return 'Записано маршрутов: ' + input.length
}

// загрузка набора данных
async function runLoadKit(kitName) {
  if (!kitName) return []
  let path = config.path['kit'] + '/' + kitName
  if (!fs.existsSync(path)) throw 'Не найден ' + path
  let data = fs.readFileSync(path)
  let ar = data.toString().split('\n')
  let arProduct = []
  let sqlProduct = ''
  for (let mix of ar) {
    let m = mix.split('\t')
    if (!m[0]) continue
    arProduct.push(m[0])
    if (sqlProduct) sqlProduct += ','
    sqlProduct += "'" + m[0] + "'"
  }
  if (arProduct.length == 0) return
  console.log(arProduct)

  let sql =   'SELECT "productId" as product, t1.rfm_before_mean as "refineBefore", t1.rfm_after_mean as "refineAfter", t1.rfm_version as "refineVersion", t1."softwareVersion" as "archivingVersion", to_char(timezone(\'utc\',t1."archivingTime_ptime"), \'YYYY-MM-DD HH24:MI:SS\') as "archivingTime",  t2.brief as input' 
            + ' from "CatalogItem" t1 left join code_input t2 on t1."inputLevel" = t2.code'
            + ' where "productId" in (' + sqlProduct + ')' 
            + ' order by t1."productId" asc'
  console.log(sql)      
  const { rows } = await dbL0.query(sql)
  return rows
}

// загрузка набора данных
async function runLoadQuery(qName) {
  if (!qName) return []
  let path = config.path['query'] + '/' + qName
  if (!fs.existsSync(path)) throw 'Не найден ' + path
  let sql = fs.readFileSync(path)
  console.log(sql.toString())      
  let dbUse = dbL0
  if (path.includes('query/l0/')) dbUse = dbL0
  else if (path.includes('query/work/')) dbUse = dbWork
  else if (path.includes('query/l2/')) dbUse = dbL2

  const { rows } = await dbUse.query(sql.toString())
  return rows
}

async function runJob(taskId, jobId, jsonData) {
  let qtask = ''
  let qjob = ''
  if (taskId) qtask = 't2.id = ' + taskId
  if (jobId) {
    if (qtask) qjob = ' and '  
    qjob += 't1.value_jobId = ' + jobId
  }
  let sql = 
      'SELECT t2.id as task_id, t1."value_jobId" as job_id' + 
      ', t1."value_input" as input' + 
      ', to_char(timezone(\'utc\',t1.value_started_ptime), \'YYYY-MM-DD HH24:MI:SS\') as start' + 
      ', to_char(timezone(\'utc\',t1.value_ended_ptime),   \'YYYY-MM-DD HH24:MI:SS\') as finish' + 
      ', t3.brief as exit_code' +  
      ', t1."value_output" as output' + 
      ', (date_trunc(\'seconds\',t1.value_ended_ptime)-date_trunc(\'seconds\',t1.value_started_ptime))::text duration' +
      ' from "MsgPair_out_jobList" t1' + 
      '   left join "MsgPair" t2 on t1.object_id=t2.id' + 
      '   left join "code_job_exit" t3 on t3.code=t1."value_exitCode"' + 
      ' where ' + qtask + qjob +
      ' order by t1."value_jobId"';
  console.log(sql)      
  const { rows } = await dbWork.query(sql)

  // если задан фильтр, то оставляем только заданные
  if (jsonData && jsonData.length > 0) { 
    for (let i=rows.length-1; i>=0; i--) {
      let row = rows[i]
      if (row.input.length != 1) continue
      if (jsonData.includes(row.input[0]))
         continue
      else rows.splice(i,1)
    }
  }

  return rows
}

async function runApproved(taskId) {
  let arApproved = []
  // берем лог мозаики - он лежит в последнем подзадании
  let taskUid = await getTaskUid(taskId)
  if (!taskUid) throw 'не найден uid'
  let job = -1
  job = await getMaxJobId(taskId)

  let path = config.interim + '/' + taskUid + '/' + job + '/interim/tamarinEsprit.log'
  if (fs.existsSync(path)) {
    var log = fs.readFileSync(path, { encoding : 'UTF-8' });

    // берем все между словами 
    // INFO: List of approved images:
    // INFO: Trying to import
    let regexp = /List of approved images:(.*?)tamarin::ProductEsprit::dataSelection is finished/gs;
    let matchAll = log.matchAll(regexp);
    matchAll = Array.from(matchAll); 
    if (matchAll.length > 0) {
      let extract = matchAll[0][1]

      // отрезаем имя Л1 из полного пути и делаем замену на Л0
      regexp = /\/([^/]*L1).*\//g;
      matchAll = extract.matchAll(regexp);
      matchAll = Array.from(matchAll); 
      for (let m of matchAll) {
        arApproved.push(m[1].replace(/L1/, 'L0'))
      }
    }
  }
  return arApproved
}

async function runRefine(taskId) {
  let qtask = 'is not null'
  if (taskId) qtask = '= ' + taskId
  // ищем все L0 в этом задании
  let sql = 
    'SELECT t1."value_output[1]" as l0' + 
      ' from "MsgPair_out_jobList" t1' + 
      '   left join "MsgPair" t2 on t1.object_id=t2.id' + 
      ' where t2.id ' + qtask + 
      ' and t1."value_output"[1] like \'%.L0\''
  console.log(sql)      
  const { rows } = await dbWork.query(sql)
  let l0 = []
  for (let row of rows) l0.push("'" + row.l0 + "'") 
  console.log(rows)
  sql = 'select "productId", rfm_before_mean, rfm_after_mean, rfm_before_stdev, rfm_after_stdev' + 
  ' from "CatalogItem" ' + 
  ' where "productId" in (' + l0.join(',') + ')' 
  console.log(sql)      
  let response = await dbL0.query(sql)
  return response.rows
}

// получить границу маршрута переданного продукта
async function runBorder(productId) {
  let border = []
  if (productId) {
    let path = await getPathArchiveL2(productId)
    if (!path) return null
    let arGbd = await getFileMaskOnPath(path, productId, 'GBD.json')

    if (!arGbd || arGbd.length == 0) return border
    //let gbd = mergeCoordsFromGbdFiles(arGbd)
    //if (!gbd || !gbd.geometry || !gbd.geometry.coordinates) return border
    // border = gbd.geometry.coordinates[0]
    for (let gbdPath of arGbd) {
      let rawdata = fs.readFileSync(gbdPath); 
      if (rawdata)  {
        let gbd = JSON.parse(rawdata); 
        if (!gbd || !gbd.features || !gbd.features[0] || !gbd.features[0].geometry || !gbd.features[0].geometry.coordinates) 
          continue
        border.push(gbd.features[0].geometry.coordinates[0])
      }
    }
  }
  return border
}

async function runCrop(taskId, jobId, productId, jsonData) {
  let arCrop = []
  if (productId) {
    // находим пути  к продуктам
    let path = await getPathArchiveL2(productId)
    if (!path) return null
    let arFile = await getFileMaskOnPath(path, productId, 'GBD.json')

    // контура задания нет
    arCrop.push({ name: taskId, crop: null })
    // по всем сценам
    for (let jfile of arFile)  {
      let cropFull = fs.readFileSync(jfile)
      let jcrop = JSON.parse(cropFull)
      let crop = jcrop.features[0].geometry.coordinates
      let k = jfile.lastIndexOf('/')
      let route = jfile.substring(k+1)
      //route = route.replace(/\.L.*/,'')
  
      arCrop.push({name: route, crop: crop })
    }
    return arCrop
  }

  // если для всего задания, то сначала ищем контур, который был передан в задании
  if (!jobId) {
    let sql = 'select "in_params_croppingShape" as crop from "MsgPair" where id=' + taskId; 
    console.log(sql)      
    const { rows } = await dbWork.query(sql)
    let crop = null
    // шейп, который был передан в задании
    if (rows && rows[0] && rows[0].crop) {
      let cropFull = rows[0].crop
      let cropFullJson = JSON.parse(cropFull)
      if (cropFullJson.features && cropFullJson.features[0] && cropFullJson.features[0].geometry)
        crop = cropFullJson.features[0].geometry.coordinates
      else
        crop = cropFullJson.coordinates
    }
    // добавляем всегда - даже если не было 
    arCrop.push({ name: taskId, crop: crop })
    //if (true) return arCrop

    let arFrg = []
    let arCropFrg = []

    // если вызов идет без указания конкретных маршрутов, то для мозаик отобразим линии порезов, 
    // хранятся в файле SML.parquet
    if (!jsonData.fileNames || jsonData.fileNames.length == 0) {
      let arParkuet = await findAllFiles(config.output, taskId, null, "SML.parquet")
      if (arParkuet && arParkuet.length > 0) {
        let park = arParkuet[0]
          
        // create new ParquetReader that reads from 'fruits.parquet`
        let reader = await parquet.ParquetReader.openFile(park);
        // create a new cursor
        let cursor = reader.getCursor();
        // read all records from the file and print them
        let record = null;
        while (record = await cursor.next()) {
          console.log(record)
          //let json = brotli.decompress(record.geometry)
          //console.log(json)
        }
      }
      for (let jfile of arFrg)  {
        let nameCrop = await allPolygonFromShape(jfile)
        arCropFrg.push(...nameCrop)
        if (arCropFrg.length > jsonData.limit) break
      }
      if (arCropFrg.length > 0 && (!jsonData.fileNames || jsonData.fileNames.length == 0) ) {
        arCrop.push(...arCropFrg)
        return arCrop
      }
      return arCrop
    }

    // Для композита 
    arFrg = await findAllFiles(config.output, taskId, null, ".FRG.json")
    for (let jfile of arFrg)  {
      let nameCrop = await allPolygonFromJson(jfile)
      arCropFrg.push(...nameCrop)
      //arCrop.push(nameCrop[0], nameCrop[1])
      if (arCropFrg.length > jsonData.limit) break
    }
    // если фрагменты облаков готовы, то покажем только их
    // остальные не будем показывать
    if (arCropFrg.length > 0 && (!jsonData.fileNames || jsonData.fileNames.length == 0) ) {
      arCrop.push(...arCropFrg)
      return arCrop
    }

    let arFile = []
    // шейпы для подзаданий 
    arFile = await findAllFiles(config.interim, taskId, '*', ".GBD.json")
    // если задан фильтр, то оставляем только заданные
    if (jsonData && jsonData.fileNames && jsonData.fileNames.length > 0) { 
      for (let i=arFile.length-1; i>=0; i--) {
        let file = arFile[i]
        let match = archiveName(file)
        if (jsonData.fileNames.includes(match)) continue
        else arFile.splice(i,1)
      }
    }
    for (let jfile of arFile)  {
      let cropFull = fs.readFileSync(jfile)
      let jcrop = JSON.parse(cropFull)
      crop = jcrop.features[0].geometry.coordinates
      arCrop.push({ name: simplify(jfile), crop: crop })
      if (arCrop.length > jsonData.limit) break
    }

    // и наконец шейп результирующего продукта
    arFile = await findAllFiles(config.output, taskId, null, ".GBD.json")
    if (arFile.length >= 0) {
      for (let jfile of arFile)  {
        let cropFull = fs.readFileSync(jfile)
        let jcrop = JSON.parse(cropFull)
        crop = jcrop.features[0].geometry.coordinates
        arCrop.push({ name: simplify(jfile), crop: crop })
        if (arCrop.length > jsonData.limit) break
      }
    }
    else  {
      // если шейпа для задания нет, то берем у мозаики gbd.shp  (json нет)
     let arFile = await findAllFiles(config.output, taskId, null, ".GBD.shp")
      for (let jfile of arFile)  {
        crop = await simplePolygonFromShape(jfile)
        arCrop.push({ name: simplify(jfile), crop: crop })
        if (arCrop.length > jsonData.limit) break
      }
    }
  }
  // если конкретное подзадание
  else if (jobId) {
    // если был контур на все задание - то его тоже покажем
    let sql = 'select "in_params_croppingShape" as crop from "MsgPair" where id=' + taskId; 
    console.log(sql)      
    const { rows } = await dbWork.query(sql)
    let crop = null
    // шейп, который был передан в задании
    if (rows && rows[0] && rows[0].crop) {
      let cropFull = rows[0].crop
      let cropFullJson = JSON.parse(cropFull)
      if (cropFullJson.features && cropFullJson.features[0] && cropFullJson.features[0].geometry)
        crop = cropFullJson.features[0].geometry.coordinates
    }
    // добавляем всегда - даже если не было 
    arCrop.push({ name: taskId + ' ' + jobId, crop:crop })

    // ДЛЯ МОЗАИКИ ПОКАЖЕМ GBD.shp FRG.shp
    ///////////////////////////////////////////
    // файл GBD.shp преобразуем в json и тоже добавим в кроп
    /*
    let arGbd = await findAllFiles(config.output, taskId, null, ".GBD.shp")
    for (let jfile of arGbd)  {
      crop = await allPolygonFromShape(jfile)
      arCrop.push(...crop)
      if (arCrop.length > jsonData.limit) break
    }
    */

    // ищем позадание в интерим - если находим, то показываем только шейп подзадания
    let arSubtask = await findAllFiles(config.interim, taskId, jobId, ".GBD.json")
    for (let jfile of arSubtask)  {
      let cropFull = fs.readFileSync(jfile)
      let jcrop = JSON.parse(cropFull)
      let crop = jcrop.features[0].geometry.coordinates
      arCrop.push({ name: null, crop: crop })
      if (arCrop.length > jsonData.limit) break
    }

    // если в интерим не нашли то в output
    if (arSubtask.length == 0) {
      // файл FRG.shp преобразуем в json и тоже добавим в кроп
      let arFrg = await findAllFiles(config.output, taskId, null, ".FRG.shp")
      for (let jfile of arFrg)  {
        crop = await allPolygonFromShape(jfile)
        arCrop.push(...crop)
        if (arCrop.length > jsonData.limit) break
      }
      // ДЛЯ ОБЫЧНОГО ПРОДУКТА ЕСТЬ ТОЛЬКО КОНТУР
      let arFile = await findAllFiles(config.output, taskId, jobId, ".GBD.json")
      for (let jfile of arFile)  {
        let cropFull = fs.readFileSync(jfile)
        let jcrop = JSON.parse(cropFull)
        let crop = jcrop.features[0].geometry.coordinates
        let name = jfile.replace(/.*\//,'')
        arCrop.push({ name: name, crop: crop })
        if (arCrop.length > jsonData.limit) break
      }
    }
  }
  return arCrop
}

function simplify(fileName) {
  let name = fileName
  let k = fileName.lastIndexOf('/')
  if (k > 0) name = fileName.substring(k+1)
  name = name.replace('.GBD.json', '')
  return name    
}


async function runMask(taskId, jobId, productId, jsonData) {
  let arCropFrg = []
  let arCrop = []
  let arFile = []

  if (taskId && jobId) {
    // если нужна маска для мозаики (находится файл FRG.shp в папак output) 
    // то запомним шейп, стобы сделать пересечение потом
    let arFrg = await findAllFiles(config.output, taskId, jobId, ".FRG.shp")
    if (arFrg) {
      for (let jfile of arFrg)  {
        let nameCrop = await allPolygonFromShape(jfile)
        arCropFrg.push(...nameCrop)
      }
    }
    
    let ext = jsonData.mask
    // сначала ищем маски в output папке (сработает для обычного задания L1,2)
    arFile = await findAllFiles(config.output, taskId, jobId, jsonData.mask)
    // если там нет, то ищем маски в interim папке (сработает для подзаданий L3)
    if (arFile.length == 0) {
      arFile = await findAllFiles(config.interim, taskId, jobId, jsonData.mask)
    }
    // если и там нет то значит пришел запрос на все маски с мозаики - уберем jobId
    if (arFile.length == 0)
      arFile = await findAllFiles(config.interim, taskId, null, jsonData.mask)
  }
  else if (productId) {
    let pathL2 = await getPathArchiveL2(productId)
    arFile = await getFileMaskOnPath(pathL2, productId, jsonData.mask)
  }

  for (let jfile of arFile)  {
    let cropFull = fs.readFileSync(jfile)
    let jcrop = JSON.parse(cropFull) 

    let name = jfile.replace(/.*\//,'')
    name = name.replace(/L.*/,'L0')
    let arFrg = arCropFrg.filter( (element) => {
      let frgName = element.name.replace(/L.*/,'L0')
      return frgName == name
    })

    let tmpCrop = []
    for (let feature of jcrop.features) {
      if (feature.properties.Role != 'Mask') continue;
      if (feature.geometry.type == 'Polygon') tmpCrop.push(feature.geometry.coordinates)
      else if (feature.geometry.type == 'MultiPolygon') {
        tmpCrop.push(...feature.geometry.coordinates)
      }
    }
      
    for (let crop of tmpCrop) {
      // если нужно пересечение с фрагментом, то проверяем каждый кроп с массивом фрагментов
      // если хотя бы один пересекся, то добавляем кроп в ответ
      if (arFrg.length > 0) {
        let frgCrop = null
        for(let frg of arFrg) {
          let poly1 = []
          poly1.push(crop)
          let poly2 = []
          poly2.push(frg.crop)

          let cropClip = polygonClipping.intersection(poly1, poly2)
          if (cropClip && cropClip.length > 0) {
              arCrop.push(cropClip[0])
            break
          }
        }
      }
      else {
        if (crop && crop.length > 0) 
          arCrop.push(crop)
      }
    }
  }  

  return arCrop
}

function getFolderMaskForScene(productId) {
  let folderMask = productId.replace('L0','L2')
  if (folderMask.startsWith('M2')) {
    let ar = folderMask.split('_')
    ar[4] = '*'
    folderMask = ar.join('_')
  }
  return folderMask
}


async function getFileMaskOnPath(pathL2, productId, mask) {
  let arFile = []
  let arProduct = []  
  if (productId) {
    // сцены от всех продуктов здесь вместе на одном уровне
    // сначала ищем все директории, которые начинаются с productId 
    let productL2 = getFolderMaskForScene(productId)
    let ar = await readdirpPromiseArray(pathL2, { type: 'directories', directoryFilter: [ productL2 + '*' ] })
    for (let path of ar) 
      arProduct.push(path.fullPath)
  }
  else 
    arProduct.push(pathL2)
  // и в каждой директории файл соотв-й маски
  for (let prod of arProduct) { 
    let scnMask = await readdirpPromiseArray(prod, { type: 'files', fileFilter: [ '*.' + mask ] })
    for (let file of scnMask) {
      arFile.push(file.fullPath)
    }
  }
  return arFile
}

async function runMake(jsonData) {
  let ret = []
  let { level, bandCombination, input, polygon, taskPrefix, folder, params, priority } = jsonData 

  new Worker('./src/fileImport.js', { workerData:
                { folder: folder, params: params, priority: priority,
                  level:level, bandCombination:bandCombination, input:input, polygon:polygon, taskPrefix:taskPrefix,
                  config: config,
                  message: 'TASK'
                }
              })
  return ''
}

export async function runDeleteItem(jsonData) {
  let { taskId } = jsonData
  let objId = []
  for (let id of taskId) {
    objId.push(id)
  }
  new Worker('./src/fileImport.js', { workerData:
    {input: objId, config: config, message: 'DELETE_ITEM'}
  })
  return ''
}

export async function runCmd(name, jsonData) {
  let { taskId } = jsonData

  let objId = []
  for (let id of taskId) {
    let uid = await getTaskUid(id)
    objId.push(uid)
  }
  new Worker('./src/fileImport.js', { workerData:
    {uid: objId, config: config, message: name}
  })
}

async function getTaskUid(taskId) {
  let qtask = 'is not null'
  if (taskId) qtask = '= ' + taskId
  let sql = 
    'SELECT t1.in_uid as uid' +
      ' from "MsgPair" t1' + 
      ' where t1.id =' + taskId;
  //console.log(sql)      
  const { rows } = await dbWork.query(sql)
  if (rows.length > 0 && rows[0].uid) return rows[0].uid
  else return null
}

async function getTaskInfo(taskId) {
  if (!taskId) return
  let fieldCode = ['"in_type"', '"in_priority"', '"in_params_level"', '"in_params_bandCombination"', '"in_params_rasterFormat"', '"in_params_compression"', '"in_params_rasterType"', '"out_taskStatus"', '"out_type"', '"out_jobStatus"']
  let field = ['"in_uid"', '"in_params_projection"', '"in_params_croppingShape"', '"in_params_useMeanHeight"', '"in_params_bandAutoAssign"', '"in_params_resolution"', '"in_params_mosaicName"', '"in_params_isPortal"', '"in_params_pullUpToRef"', '"in_info_email"', '"in_info_number"', '"in_info_request"', '"in_info_customer"', '"in_info_department"', '"in_info_organization"', '"out_uid"', '"out_msg"', '"out_created_ptime"', '"out_started_ptime"', '"out_ended_ptime"', '"id"']
  let select = ''
  for (let item of fieldCode) {
    if (select) select += ','
    select += item + ' as "' + item.substring(1,item.length-1) + '_code"' 
  }
  for (let item of field) {
    if (select) select += ','
    select += item 
  }

  let sql = 'SELECT ' + select + ' from "MsgPair" where id =' + taskId;
  console.log(sql)      
  const { rows } = await dbWork.query(sql)
  replaceCode(rows)
  if (rows.length == 1) return rows[0]
  else return null
}


async function getMaxJobId(taskId) {
  let qtask = 'is not null'
  if (taskId) qtask = '= ' + taskId
  let sql = 
    'SELECT max("value_jobId") as job'   +
      ' from "MsgPair" t1 inner join "MsgPair_out_jobList" t2 on t1.id=t2.object_id' + 
      ' where t1.id =' + taskId;
  console.log(sql)      
  const { rows } = await dbWork.query(sql)
  if (rows.length > 0 && rows[0].job) return rows[0].job
  else return null
}


async function runLog(taskId, jobId, name) {
  let ret = ''
  let data = "";
  if (!taskId) return `Не задан taskId`
  let taskUid = await getTaskUid(taskId)
  if (!taskUid) return `Не найден uid для taskId=${taskId}`
  if (!jobId) jobId = ''
  let folderInterim =  config.interim + "/" + taskUid + '/' + jobId
  let folderOutput =  config.output + "/" + taskUid + '/' + jobId
  let logName = '.log'
  // вытаскиваем только лог редута
  if (!jobId) {
    logName = 'Redoute.log'
  }
  let arFile = []
  for await (const entry of readdirp(folderInterim, { fileFilter: [ '*' + logName ], depth: 2  }) ) {
    arFile.push(path.join(folderInterim,entry.path))
  }
  if (arFile.length == 0) {
    for await (const entry of readdirp(folderOutput, { fileFilter: [ '*' + logName ], depth: 2  }) ) {
      arFile.push(path.join(folderOutput,entry.path))
    }
  }
  if (arFile.length == 0) return `Не найден log файл в папке ${folderInterim} или ${folderOutput}`
  arFile.sort();
  for (let file of arFile) {
    data+=file
    data+='\n'
    data += fs.readFileSync(file)
  }
  ret = data
  // показываем не весь лог, а только ошибки
  if (name) {
    ret = ''
    let lines =  data.split('\n')
    for (let line of lines) {
      if (line.includes(name + ':')) ret += (line + '\n')
    }
  }

  // добавим в конец файла содержимое error/uid* 
  let folder =  config.error
  let errJob = ''
  if (jobId) errJob += '-' + jobId
  arFile = await readdirpPromiseArray(folder,{ type: 'files', fileFilter: [ taskUid + errJob + '*.log' ] })
  for (let file of arFile) {
    let d = fs.readFileSync(file.fullPath)
    if (d.length == 0) continue
    ret += '\n\n'
    ret += file.fullPath
    ret += '\n'
    ret += d
  }

  // если лог для всего задания, то добавим информацию по заданию
  if (!jobId) {
    ret += "\n\n"
    let tinfo = await getTaskInfo(taskId)
    
    let sorted = Object.keys(tinfo)
    .sort()
    .reduce(function (acc, key) { 
        acc[key] = tinfo[key];
        return acc;
    }, {});
    for (let key in sorted) {
      ret += key.padEnd(30) + sorted[key] + '\n'
    }
 
  }

  return ret;
}


async function runFile(name) {
  let ret = ''
  ret = fs.readFileSync(name)
  return ret;
}

async function runStand(jsonData) {
  initConfig({name:version})
}

async function runConfig(jsonData) {
  let {level, ka, camera} = jsonData
  let rKa = ka.replace('R','RESURS_')
  rKa = rKa.replace('K','KANOPUS_')
  rKa = rKa.replace('KANOPUS_VI','KANOPUS_VIK')
  rKa = "[satelliteList/satellite='" + rKa + "']"
  rKa = rKa.replace('M2', 'METEOR_M2')
  let rLevel = level.replace('L1','L2')
  if (rLevel)
    rLevel = "[typeList/type[substring(text(), string-length(text()) - string-length('" + level + "') +1) = '" + level +"']]"

  let rCamera = ''
  if (camera && !ka.includes('KV') && !level.includes('RAW') && !level.includes('L3M') && !level.includes('L3C'))
   rCamera="[deviceList/device='" + camera + "']"
 
  let ret = ''
  try {
    let file =  config.configPath + '/configRedoute.xml'
    ret += '\n' 
    // строка слурма
    ret += readConfig("/tamarinRedouteConfig/processingChainList/processingChain" + rKa + "/commandList/command" + rLevel + rCamera + "/prefixList/prefix[@type='SLURM']/text()", file)
    ret += '\n' 
    // команда на выполнение 
    let cmd = readConfig("/tamarinRedouteConfig/processingChainList/processingChain" + rKa + "/commandList/command" + rLevel + rCamera + "/text/text()", file)
    ret += cmd
    // добавка по разным условиям
    let arCond = ['product', 'product_byte', 'portal', 'l3']
    for(let cond of arCond) { 
      let vCond = readConfig("/tamarinRedouteConfig/processingChainList/processingChain" + rKa + "/commandList/command" + rLevel + rCamera + "/if[@value='" + cond + "']/text()", file)
      let regexp = /\n([ ]*)--/
      let match = vCond.match(regexp)
      if (!match) continue
      if (match && match[1]) 
        cond = match[1] + cond
      ret += cond
      vCond = vCond.replaceAll(match[1],match[1] + '  ')
      ret += vCond
    }
    var arg = minimist(cmd.split(' '))
    // файлы конфигов
    let arFile = []
    // конфиг куксы
    let cnfKuksa = arg['config-file']
    // конфиг качества
    let cnfQuality = arg['qa-config']
    // опора для Л2
    let rrr = arg['reference']
    if (rrr) {
      if (Array.isArray(rrr))  arFile.push(...rrr)
      else arFile.push(rrr)
    }

    if (cnfKuksa) {
      cnfKuksa = cnfKuksa.replaceAll('\t',' ').replaceAll('\r','').replaceAll('\n','')
      let cmd = readConfig("/processingChainList/processingChain[case='DEFAULT']" + rKa + rCamera + "/commandList/command/text()", cnfKuksa, level)
      ret += '\n' + cmd
      cmd = cmd.replaceAll('\t',' ').replaceAll('\r','').replaceAll('\n','')
      arg = minimist(cmd.split(' '))
    }
    // конфиг тейи
    arFile.push(arg['config-file'])
    // опора для Л3
    let r = arg['reference']
    if (r) {
      if (Array.isArray(r)) arFile.push(...r)
      else arFile.push(r)
    }
    // конфиг качества
    arFile.push(cnfQuality)
    let uniq = []
    for(let cnf of arFile) {
      if (cnf) {
        cnf = cnf.replaceAll('\t',' ').replaceAll('\r','').replaceAll('\n','' )
        if (uniq.includes(cnf)) continue
        cmd = readConfig("/", cnf)
        //ret += '\n' + cnf
        ret += '\n' + cmd
        uniq.push(cnf)
      }
    }
  }
  catch (e) {
    ret = e.toString()
  }
  return ret
}

async function runImage(name) {
  let ret = null
  if (fs.existsSync(name))
    ret = fs.readFileSync(name)
  return ret;
}

async function getPathArchiveL2(productId) {
  let ret = null
  if (!productId) return ret
  // находим путь к продукту по каталогу Л0
  // id продукта может быть сценой из архива Л2, но там пути не хранятся, но по смыслу такие же как в каталоге Л0
  try {
    let productL0 = productId.replace(/\.L.*/,'\.L0')
    let sql = 'select "productId",path from "CatalogItem" where "productId"=\'' + productL0 + '\''
    let { rows } = await dbL0.query(sql)
    if (rows.length == 0) return ret
    if (!rows[0].path) return ret
    let k = rows[0].path.lastIndexOf('/')
    ret = rows[0].path.substring(0,k)
    // место в архиве Л2 просто заменой корня
    ret = ret.replace(config.archiveL0, config.archiveL2)
    // если из архива Л2 берем сцену, то конкретную папку со сценой возвращаем
//    if (productId.includes('.SCN')) {
//      ret += '/' + productId
//    } 
  }
  catch(e) {
    console.log(e)
  }
  return ret
}

async function runImages(taskId, jobId, productId, jsonData) {
  let arQL = []  
  let arQA = []  
  let arProductL2Path = []
  let arFile = []
  let pList = []
  let {productList } = jsonData
  if (productId) pList.push(productId)
  if (productList) pList.push(...productList)
  if (pList.length > 0) {
    for (let i=0; i<pList.length; i++) {
      let pId = pList[i]
      // показываем изображение продукта из каталога Л2
      let path = await getPathArchiveL2(pId)
      if (!path) return null
      let ar1 = await getFileMaskOnPath(path, pId, 'QL.jpg')
      let ar2 = await getFileMaskOnPath(path, pId, 'quicklook*.jpg')
      if (ar1.length > 0) arQL.push(...ar1)
      if (ar2.length > 0) arQL.push(...ar2)
    }
  }
  else {
    let taskUid = await getTaskUid(taskId)
    let jobba = jobId ? '/' + jobId : '' 
    // сначала ищем в output директории задания - если не нашли - в interim
    let path = config.output + '/' + taskUid + jobba
    let ar = await getFileMaskOnPath(path, productId, 'QL.jpg')
    if (!ar) {
      path = config.interim + '/' + taskUid + jobba
      ar = await getFileMaskOnPath(path, productId, 'QL.jpg')
    }
    if (ar.length > 0) arQL.push(...ar) 
  }
  arQL.sort()
  let path = []
  initPathMask(path)
  let ret = []
  for(let i=0; i<arQL.length; i++) {
    let qlPath = arQL[i]
    let obj = { path: qlPath }
    // берем папку в которой лежит квиклук
    let k = qlPath.lastIndexOf('/')
    let qaFolder = qlPath.substring(0,k)
    // и в ней ищем QA
    let arQA = await readdirpPromiseArray(qaFolder, { type: 'files', fileFilter: [ '*.QA.xml' ] })
    // должны найти 1
    if (arQA.length == 1) {
      let qaPath = arQA[0].path
      let keyQA = qaFolder + '/' + qaPath
      obj.qa = keyQA
      let k = qaPath.lastIndexOf('/')
      obj.name = qaPath.substring(k+1).replace('.QA.xml','')

      for (let i=0; i<path.length; i++) {
        let r = path[i].path
        let value = readValue(r, keyQA)
        obj[path[i].name] = value
      }
    }
    ret.push(obj)
  }
  return ret
}

// Ищем файлы включающие заданное имя для задания (папка output) и операции (папка interim) 
// возвращаем полные пути
async function findAllFiles(root, taskId, jobId, name, dFilter) {
  if (!taskId) throw `Не задан taskId`
  let taskUid = await getTaskUid(taskId)
  if (!taskUid) throw `Не найден uid для taskId=${taskId}`
  let folder =  root + '/' + taskUid
  if (jobId && jobId != '*') {
    folder += '/' + jobId;
  }
  
  if (!dFilter) dFilter = [ '!interim' ]
  let options = {
    fileFilter: [ '*' + name ],
    directoryFilter: dFilter
  };
  let arPath = []

  let arGBD = await readdirpPromiseArray(folder, options)
  if (!arGBD || arGBD.length == 0) return arPath
  for(let gbd of arGBD)
    arPath.push(path.join(folder,gbd.path))

    /*
  for await (const entry of readdirp(folder, options) ) {
    arPath.push(path.join(folder,entry.path))
  }
  */
  console.log(arPath)
  return arPath;
}

function simplePolygonFromShape(shapeFile)  {
  let promise = new Promise( (resolve,reject) => {
    shapefile.read(shapeFile, (err,json) => {
      let xxx = []
      let union 
      if (json && json.features) {
        union = makeUnion(json.features, union)
        xxx.push(union.geometry.coordinates[0])
      }
      resolve(xxx)
    })
  })
  return promise
}

function allPolygonFromShape(shapeFile)  {
  let promise = new Promise( (resolve,reject) => {
    shapefile.read(shapeFile, (err,json) => {
      let xxx = []
      if (json && json.features) {
        for (let feature of json.features) {
          if (feature.geometry && feature.geometry.coordinates) {
            let options = {tolerance: 0.001, highQuality: false};
            feature = turf.simplify(feature, options);
            for (let pol of feature.geometry.coordinates) {
              let a = []
              // не получается по типу отделить - такой вот костыль получился
              if (pol.length == 1)
                a.push(...pol)
              else 
                a.push(pol)
              //console.log(a)
              xxx.push({ name: feature.properties.RasterID, crop: a })
            }
          }
        }
      }
      resolve(xxx)
    })
  })
  return promise
}

function allPolygonFromJson(file)  {
  let promise = new Promise( (resolve,reject) => {
    fs.readFile(file, (err,data) => {
      let xxx = []
      let json = JSON.parse(data)
      if (json && json.features) {
        for (let feature of json.features) {
          if (feature.geometry && feature.geometry.coordinates) {
            let options = {tolerance: 0.001, highQuality: false};
            feature = turf.simplify(feature, options);
            for (let pol of feature.geometry.coordinates) {
              let a = []
              if (pol.length == 0) continue
              // не получается по типу отделить - такой вот костыль получился
              if (pol.length == 1)
                a.push(...pol)
              else 
                a.push(pol)
              //console.log(a)
              xxx.push({ name: feature.properties.RasterID, crop: a })
            }
          }
        }
      }
      resolve(xxx)
    })
  })
  return promise
}

function server()  {
  initConfig()
  const server = http.createServer(requestListener);
  console.log('Started on port ' + config.port + ' ...')
  process.on('uncaughtException', function (err) {
    console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
    console.error(err.stack)
  })
  server.listen(config.port);
}

// Сформировать границы маршрутов, которых еще нет в каталоге
async function runGeoborder(productId)  {
  await initDb()
  // находим имена маршрутов с путями к ним где не проставлен геобордер
  let prod = ''
  if (productId) prod = ' and \"productId\" = \'' + productId + '\''
  let sql = 'select "productId",path ' + 
    ' from "CatalogItem" ' + 
    ' where geoborder is null' + prod
    console.log(sql)      
    let response = await dbL0.query(sql)
    let count = 0
    // по всем найденным
    for( let row of response.rows)  {
      let productId = row.productId
      console.log(productId)
      // Берем сцены из каталога Л2
      let path = await getPathArchiveL2(productId)
      if (!path) return null
      let arGbd = await getFileMaskOnPath(path, productId, 'GBD.json')

      if (!arGbd || arGbd.length == 0) continue
      let gbd = mergeCoordsFromGbdFiles(arGbd)
      if (!gbd || !gbd.geometry || !gbd.geometry.coordinates) continue
      let coord = gbd.geometry.coordinates
      // для sql нужно так '((0,1),(1,2),(2,3))'
      // а в программе и так подходит
      let gbdData = String(coord)
      let sqlInsert = 'update "CatalogItem" set geoborder = \'' + gbdData + '\' where "productId" = \'' + productId + '\''
      dbL0.query(sqlInsert)
      count++
    }
    return 'Обновлено маршрутов: ' + count
}

// Сформировать квиклуки маршрутов, которых еще нет в каталоге
async function runQL()  {
  await initDb()
  let sql = 'select "productId",path ' + 
    ' from "CatalogItem" ' + 
    ' where ql is null'
    console.log(sql)      
    let response = await dbL0.query(sql)
    let count = 0
    for( let row of response.rows)  {
      let productId = row.productId
      let startQL = makeCmdQL(path.join(row.path,productId + '.MD.xml'), config.qlFolder)
      let product = productId.replace(/.L0/g, '') 
      let qlBasePath = config.qlFolder + '/' + product
      let qlPan = qlBasePath + '.L1.PAN'
      let qlMs = qlBasePath + '.L1.MS'
      qlPan = qlPan + '/' + product + '.L1.PAN' + '.QL.jpg'
      qlMs = qlMs + '/' + product +  '.L1.MS' + '.QL.jpg'

      if (fs.existsSync(qlMs))
        onFinishQL(qlMs, productId)
      else if (fs.existsSync(qlPan))
        onFinishQL(qlPan, productId)
      else {
        console.log(startQL)
        exec(startQL)
      }
      count++
    }
    return 'Запущено обновление маршрутов: ' + count
}
function onFinishBorder(borderPath, productId) {
  if (!borderPath || !productId || !fs.existsSync(borderPath)) {
    return; 
  }
  let rawdata = fs.readFileSync(borderPath); 
  if (rawdata)  {
    let gbd = JSON.parse(rawdata); 
    let coord = gbd.features[0].geometry.coordinates
    // '((0,1),(1,2),(2,3))'
    let sqlInsert = 'update "CatalogItem" set geoborder = \'' + String(coord) + '\' where "productId" = \'' + productId + '\''
    dbL0.query(sqlInsert)
   }
}




function onFinishQL(qlPath, productId) {
  if (!qlPath || !productId || !fs.existsSync(qlPath)) {
    return; 
  }
  let sqlInsert = 'update "CatalogItem" set ql = \'' + qlPath + '\' where "productId" = \'' + productId + '\''
  dbL0.query(sqlInsert)
}


function makeCmdGeoborder(input, output) {
  return config.ssh + ' \'' + config.borderTerminal + ' ' + ' --input ' + input + ' --output ' + output + '\''
}

function makeCmdQL(input, output) {
  return config.ssh + ' \'' + config.qlTerminal + ' ' + ' --input ' + input + ' --output ' + output + '\''
}

function runPolygon(jsonData) {
  var obj = JSON.parse(fs.readFileSync(jsonData.path, 'utf8'));
  let geometry = null
  if (obj && obj.features && obj.features[0] && obj.features[0].geometry) geometry = obj.features[0].geometry
  else if (obj.coordinates) geometry = obj
  return geometry
}

function extractRegion()  {
  let count = 0
  let countAll = 0
  let root = '/home/omega/projects/javascript/redoute'
  var obj = JSON.parse(fs.readFileSync(root + '/region.geojson', 'utf8'));
  for (let feature of obj.features) {
    if (feature.geometry.coordinates.length == 1) 
    {
      let region = { "type": "FeatureCollection", features: [ {type: "Feature", geometry: { type: "Polygon", coordinates: feature.geometry.coordinates[0]} } ] }
      let fname  = feature.properties.title_en.replace(/ /g, '_') 
      fname+='.geojson'
      console.log(feature.properties.title_en + ' ' + JSON.stringify(region))
      fs.writeFileSync(root + '/region/' + fname, JSON.stringify(region))
    }
    else {
      let arcoord = [] 
      for (let c of feature.geometry.coordinates) {
        arcoord.push(c)
      }
      let region = { "type": "FeatureCollection", features: [ {type: "Feature", geometry: { type: "MultiPolygon", coordinates: arcoord } } ] }
      let fname  = feature.properties.title_en.replace(/ /g, '_') 
      fname+='.geojson'
      console.log(feature.properties.title_en + ' ' + JSON.stringify(region))
      fs.writeFileSync(root + '/region/' + fname, JSON.stringify(region))
    }
  }
}

function archiveName(file) {
    // отрезаем имя Л1 из полного пути и делаем замену на Л0
    let regexp = /\/([^/]*L[12]).*\//
    let match = file.match(regexp)
    if (match && match[1]) 
      match = match[1].replace(/L[12]/, 'L0')
    return match
}

// поиск в переданных именах всех QA. xml в interim, output
// сохранил старый код на всякий случай
async function runQualityApproved(taskId, jobId, jsonData) {
  let ret = []
  // ищем QA в output - любой
  let arFileOutput = await findAllFiles(config.output, taskId, jobId, 'QA.xml')
  // или QA в interim - Л1 Л2 
  let arFileInterimL1 = await findAllFiles(config.interim, taskId, jobId, 'L1*QA.xml')
  let arFileInterimL2 = await findAllFiles(config.interim, taskId, jobId, 'L2*QA.xml')
  // все в массив и сортируем по полному имени
  let arFile = []
  arFile.push(...arFileOutput)
  arFile.push(...arFileInterimL1)
  arFile.push(...arFileInterimL2)
  arFile.sort()

  arFile.splice(jsonData.limit)
  // что будем вытаскивать из хмл
  let path = []
  initPathMask(path)

  // цикл по всем файлам
  for(let file of arFile) {
    // фильтр на данные в переданных именах
    if (jsonData && jsonData.fileNames.length > 0 && !jsonData.fileNames.includes(archiveName(file))) continue
    // оставляем только имя
    let k = file.lastIndexOf('/')
    let shortFile = file
    if (k != -1) 
     shortFile = file.substring(k+1)
    // формируем результат [ {file:, name:, value:, path} ]
    for (let r of path) {
      let value = readValue(r.path, file)
      ret.push( {file: shortFile, name: r.name, value: value, path: r.path })
    }
  }
  return ret
}

// поиск в output  
async function runQuality(taskId, jobId, productId, jsonData) {
  let ret = []
  let arFile = []
  let quality = []
  let itog = []

  let arQa = []

  if (taskId) {
    // ищем QA в output - любой
    arQa = await findAllFiles(config.output, taskId, jobId, 'QA.xml')
    // только для мозаик делаем
    for(let qa of arQa) {
      if (qa.includes('MSC'))
        arFile.push(qa)
    }
    initPathRefine(quality)
  }
  else if (productId) {
    // ищем в архиве продукт Л2 и из него все QA
    // сначала из базы путь 
    let sql = 'select "productId",path from "CatalogItem" where "productId"=\'' + productId + '\''
    console.log(sql)      
    const { rows }  = await dbL0.query(sql)
    // здесь лежат Л2 ПМС с порезкой на сцены и из них берем QA
    let productL2Path = null
    if (rows.length > 0) 
      productL2Path = rows[0].path + '_product'

    // 
    arQa = await readdirpPromiseArray(productL2Path,{ type: 'files', fileFilter: [ '*.QA.xml' ] })
    for(let qa of arQa)
      arFile.push(productL2Path + '/' + qa.path)
    arFile.sort()
    // в архиве лежит порезанный на сцены Л2 - из QA.xml берем маски
    initPathMask(quality)
  }

  if (jsonData.limit) arFile.splice(jsonData.limit)

  // цикл по всем файлам
  for(let file of arFile) {
    // фильтр на данные в переданных именах
    let k = file.lastIndexOf('/')
    let shortFile = file
    if (k != -1) 
     shortFile = file.substring(k+1)

    // формируем результат [ {file:, name:, value:, path} ]
    if (taskId) {
      // специфика уточнения в мозаике
      let allRow = []
      for (let i=0; i<quality.length; i++) {
        allRow[i] = []
        for (let j=0; j<quality[i].name.length; j++) {
          let qName = quality[i].name[j]
          let qPath = quality[i].path[j]
          let value =   readValue(qPath, file)
          // при добавлении первого массива сразу создаем все строки
          if (Array.isArray(value)) {
            if (allRow[i].length == 0) allRow[i] = new Array(value.length)
            for (let k=0; k<value.length; k++) {
              if (!allRow[i][k]) allRow[i][k] = {}
              let data = value[k].data
              let vvv = parseFloat(data)
              if (!isNaN(vvv) && !Number.isInteger(vvv)) {
                data = vvv.toFixed(2)
              }
              allRow[i][k][qName] = data
            }
          }
          else {
            if (allRow[0].length == 0) allRow[i].length = 1
            if (!allRow[0][0]) allRow[0][0] = {}
            let data = value
            let vvv = parseFloat(data)
            if (!isNaN(vvv) && !Number.isInteger(vvv)) {
              data = vvv.toFixed(2)
            }
            allRow[0][0][qName] = data
          }
        }
      }
      for (let i=0; i<allRow.length; i++) {
        let row = allRow[i]
        ret.push(...row)
        // добавим итоговую строку - среднее по всем значениям в столбце
        let itog = {}
        itog['product'] = 'Среднее'
        for (let j=0; j<row.length; j++) {
          let rrr = row[j]
          for (let r in rrr) {
            if (r == 'product') 
              continue;
            if (!itog[r]) 
              itog[r] = Number('0')
            itog[r] = itog[r] + Number(rrr[r])
          }
        }

        for (let r in itog) {
          if (r == 'product') 
            continue;
          itog[r] = (itog[r]/row.length).toFixed(2)
        }
        ret.push(itog)
      }
    }
    else if (productId) {
      for (let i=0; i<quality.length; i++) {
        let r = quality[i].path
        let value = readValue(r, file)
        ret.push( {file: shortFile, name: quality[i].name, value: value , path: r })
      }

    }
  }
  return ret
}

function initPathMask(path) {
  let qaTemplate = "/qa:qualityAnalysis/qa:result/qa:maskList/qa:mask[qa:type='XXXXX']/qa:areaPercentage/text()" 
  path.push( {name: 'Cloud',      path: qaTemplate.replace('XXXXX','Cloud')} )
  path.push( {name: 'Defects',    path: qaTemplate.replace('XXXXX','Defects')} )
  path.push( {name: 'DDefects',    path: qaTemplate.replace('XXXXX','DataDefects')} )
  path.push( {name: 'Snow',       path: qaTemplate.replace('XXXXX','Snow')} )
  path.push( {name: 'Shadows',    path: qaTemplate.replace('XXXXX','Shadows')} )
  path.push( {name: 'Water',      path: qaTemplate.replace('XXXXX','Water')} )
  path.push( {name: 'StitchingMs', path: qaTemplate.replace('XXXXX','StitchingMs')} )

//  path.push( {name: 'productQuality',      path: "/qa:qualityAnalysis/qa:productQualityStatus/text()"} )
  path.push( {name: 'productQualityStatus',     path: "/qa:qualityAnalysis/qa:productQualityStatus/text()"} )
  path.push( {name: 'routeQualityStatus',       path: "/qa:qualityAnalysis/qa:routeQualityStatus/text()"} )
  path.push( {name: 'summaryQualityStatus',     path: "/qa:qualityAnalysis/qa:summaryQualityStatus/text()"} )
}

function initPathRefine(allPath) {

  let names = ['product', 'count', 'L0beforeRFM_m', 'L0afterRFM_m', 'L1beforeBA_m', 'L1afterBA_m', 'L1beforeBA_bind_px', 'L1afterBA_bind_px']
  // по каждому маршруту
  /*
  L0beforeRFM_m       - привязка Л0 до уточнения, м
  L0afterRFM_m        - привязка Л0 после уточнения, м
  L1beforeBA_m        - привязка Л1 до блочного уравнивания, м
  L1afterBA_m         - привязка Л1 после блочного уравнивания, м
  L1beforeBA_bind_px  - сведение маршрутов Л1 до блочного уравнивания, пикс
  L1afterBA_bind_px   - сведение маршрутов Л1 после блочного уравнивания, пикс
  */

  allPath.push({name: names,
    path: [
      "/qualityAnalysis/routesList/route/identifier/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='FINE' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='before']/pointsStatisticsList/pointsStatistics[@dsc='input' and @type='GCP']/count/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='FINE' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='before']/pointsStatisticsList/pointsStatistics[@dsc='filtered' and @type='GCP']/mean/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='FINE' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='after']/pointsStatisticsList/pointsStatistics[@dsc='regular' and @type='GCP']/mean/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='BUNDLE_ADJUST' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='before']/pointsStatisticsList/pointsStatistics[@dsc='filtered' and @type='GCP']/mean/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='BUNDLE_ADJUST' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='after']/pointsStatisticsList/pointsStatistics[@dsc='filtered' and @type='GCP']/mean/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='BUNDLE_ADJUST' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='before']/pointsStatisticsList/pointsStatistics[@dsc='filtered' and @type='BIND_SENSOR']/mean/text()",
      "/qualityAnalysis/routesList/route/refinementList/refinement[refinementType='BUNDLE_ADJUST' and refinementStatus='SUCCESS']/estimationList/estimationSensor/estimation[@dsc='after']/pointsStatisticsList/pointsStatistics[@dsc='filtered' and @type='BIND_SENSOR']/mean/text()"
    ]})
  // по всей мозаике
/*  
  allPath.push({name: names,
    path: [
       "/qualityAnalysis/identifier/text()",
       "",
       "",
       "/qualityAnalysis/refinement/refinementList[refinementLevel='L0']/estimationList/estimationSensor/estimation[@dsc='after']/pointsStatisticsList/pointsStatistics[@type='GCP']/mean/text()",
       "/qualityAnalysis/refinement/refinementList[refinementLevel='L1']/estimationList/estimationSensor/estimation[@dsc='after']/pointsStatisticsList/pointsStatistics[@type='GCP']/mean/text()",
       "/qualityAnalysis/refinement/refinementList[refinementLevel='L1']/estimationList/estimationSensor/estimation[@dsc='after']/pointsStatisticsList/pointsStatistics[@type='BIND_SENSOR']/mean/text()",
    ]})
*/    
 }

async function  runTile(rurl) {
  const url = new URL("http:/localhost" + rurl)
  let x = url.searchParams.get("x")
  let y = url.searchParams.get("y")
  let z = url.searchParams.get("z")
  let taskId = url.searchParams.get("task_id")
  let jobId = url.searchParams.get("job_id")
  let productId = url.searchParams.get("product_id")

  let arProduct = await getProductPath(taskId, jobId, productId)
  if (arProduct.length == 0) {
    console.log('Не найден продукт для ' + taskId, ' ' , + jobId + ' '+ productId)
    return null
  }
  // определим тип данных по первому продукту (обычно здесь сцены)
  let path = arProduct[0].fullPath
  let tif = await fromFile(path);
  let image = await tif.getImage() 
  let cog = image.isTiled
  tif.close()

  // при вызове из каталога передается productId
  if (cog) 
    return await getCogTile(z,x,y,arProduct)
  //else
  //  return getGdalTile(z,x,y,taskId, jobId, arProduct)
}

let mapPathProductL2 = new Map()

async function getProductPath(taskId, jobId, productId) {
  let arProduct = []
  if (productId) {
    // находим пути к продуктам L2
    let pathL2 = await getPathArchiveL2(productId)
    // сцены от всех продуктов здесь вместе
    // сначала ищем все директории, которые начинаются с productId 
    let productL2 = await getFolderMaskForScene(productId)
    let arDir = await readdirpPromiseArray(pathL2, { type: 'directories', directoryFilter: [ productL2 + '*' ] })
    for (let dir of arDir) {
      let arTif = await readdirpPromiseArray(dir.fullPath, { type: 'files', fileFilter: [ '*.tif' ] })
      arProduct.push(...arTif)
    }
  }
  else if (taskId) {
    if (!taskId) return `Не задан taskId`
    let taskUid = await getTaskUid(taskId)
    if (!taskUid) return `Не найден uid для taskId=${taskId}`
    if (!jobId) jobId = ''
    let outputFolder =  config.output + "/" + taskUid + '/' + jobId

      // данные в выходной папке - мозаика или просто продукт
      let arP = await readdirpPromiseArray(outputFolder, { type: 'files', fileFilter: [ '*.tif' ] })
      for (let p of arP) {
        if (p.fullPath.includes('.OV.tif')) continue
        arProduct.push(p)
      }
      // промежуточные данные для мозаики
      if (arProduct.length == 0) { 
        let interimFolder =  config.interim + "/" + taskUid + '/' + jobId
        arProduct = readdirpPromiseArray(interimFolder, { type: 'files', fileFilter: [ '*.tif' ] })
      }
  }
  return arProduct
}

async function getGdalTile(z,x,y,taskId, jobId, arProduct) {
    if (!taskId) return `Не задан taskId`
    let taskUid = await getTaskUid(taskId)
    if (!taskUid) return `Не найден uid для taskId=${taskId}`
    if (!jobId) jobId = ''
    let outputFolder =  config.output + "/" + taskUid + '/' + jobId

    // для тайлов у нас отдельный корень   config.archiveL2/tiles 

    // если маршрут резался на тайлы (есть папка) просто берем тайлы
    let tilesProductRoot = config.archiveL2 + '/' + 'tiles' + '/' + taskUid + '/' + jobId
    if (fs.existsSync(tilesProductRoot) )  {
      // мозаика хранится сразу после jobId - ищем все папки для уровня z
      let arZ = await readdirpPromiseArray(tilesProductRoot, { type: 'directories', directoryFilter: [ z ] })
      // если была пакетная обработка - то после jobId идут сцены
      if (arZ.length == 0)
        arZ = await readdirpPromiseArray(tilesProductRoot, { type: 'directories', directoryFilter: [ 'SCN*', z ] })         //  в массиве передаются папки, по которым рекурсия
      // если пытаемся увидеть данные подзадания, то после jobId идет папка с продуктом
      if (arZ.length == 0)
        arZ = await readdirpPromiseArray(tilesProductRoot, { type: 'directories', directoryFilter: [ '*L1*', z ] })         //  в массиве передаются папки, по которым рекурсия
      // смотрим конкр-й тайл
      let arNeed = []
      for (let zPath of arZ) {
        let f = zPath.fullPath + '/' + x + '/' + y + '.png'
        if (fs.existsSync(f))  
          arNeed.push(f)
      }
      // 
      let resFile = null
      if (arNeed.length == 0) return null
      if (arNeed.length == 1) resFile = arNeed[0]
      else  {
        resFile = config.tmpDir + '/' + 'merge.png'
        images(arNeed[0]).save(resFile)
        for (let i=1; i<arNeed.length; i++) {
          images(resFile).draw(images(arNeed[i]), 0, 0).save(resFile);
        }
      }
      if (resFile) {
        return fs.readFileSync(resFile)
      }
      
      else return null
    }

    // режем тифы на тайлы - 
    try {
      // сразу создаем директорию как факт того, что запускали тайлер, иначе будут дублироваться
      start('mkdir -p ' + tilesProductRoot)
      for(let prod of arProduct) {
        // формируем тайлы
        makeTiles(prod.fullPath, tilesProductRoot, z)
      }
    }
    catch(e) {
      console.log(e)
      start('rm -r ' + tilesProductRoot)
    }  
}

async function makeTiles(tif, tilesRoot, level) {
  // формируем корень для тайлов одной сцены
  let k = tif.lastIndexOf('/')
  let tifByte = tilesRoot + '/' + tif.substring(k+1) + '_8.vrt'
  let fname = tif.substring(k+1)
  let k1 = fname.lastIndexOf('SCN')
  let k2 = fname.lastIndexOf('.tif')
  let tilesForScene = tilesRoot + '/' + fname.substring(k1,k2)

  // максимальный уровень 
  let upLevel = 12
  // Для РП ну очень долго - оставим только 
  if (fname.startsWith('RP')) upLevel = 5
  // Для метеора достаточно 
  if (fname.startsWith('M2')) upLevel = 8

  // для перевода в 8 бит нужно знать гистограмму и задать -scale_1 <мин> <макс> 0 255 -scale_2 <> <> 0 255
  // и оставить только ргб каналы -b 1 -b 2 -b 3
  let scale = ''
  let dir = tif.substring(0, k+1)
  let overviewAr = await readdirpPromiseArray(dir,{ type: 'files', depth: 0, fileFilter: [ '*tif' ] })
  if (false) {
    if (overviewAr.length > 0) {
      let overview = overviewAr[0].fullPath
      let overviewTif = await fromFile(overview);
      const image = await overviewTif.getImage();
      const [ red, green, blue] = await image.readRasters();
      let band  = []
      // отрезаем по площади 1% по гистограмме
      let cut = 0.01
      if (red) band.push(cutBand(red, cut))
      if (green) band.push(cutBand(green, cut))
      if (blue) band.push(cutBand(blue, cut))
      for (let i = 0; i< band.length; i++) {
        let b = band[i]
        if (!b || Object.keys(b).length == 0) continue
        scale += ' -scale_' + (i+1) + ' ' + b.minCut + ' ' + b.maxCut + ' 0 255 -b ' + (i+1)
      }
    }
  }

  // у композита нет информации о SKS. Возможно в параметрах gdal_translate это можно передать, но пока не получилось
  let cmd0 = ''
  let tif_sks = tif

  //Прочитать из MD.xml проекцию
  let sks = ''
  let md = tif.replace('.tif', '.MD.xml')
  let data = fs.readFileSync(md, { encoding : 'UTF-8' })
  if (data) { 
    data = data.toString()
    let m = data.match(/<gen:projection>(.*?)<\/gen:projection>/)
    if (m && m.length > 0 && m[1]) sks = '-s ' + m[1]
  }

  if (tif.includes('.L3C.')) {
    tif_sks = tilesRoot + '/' + tif.substring(k+1) + '_sks.vrt'
    cmd0 = 'gdalwarp -t_srs "EPSG:4326" ' + tif + ' ' + tif_sks + ';   '
  }
  let cmd1 = cmd0 +  'gdal_translate -of VRT -ot Byte ' + scale + ' ' +  tif_sks + ' ' + tifByte 
  let cmd2 = ''
  for (let i=0; i<=upLevel; i++) {
    cmd2 += 'gdal2tiles.py ' + sks + ' -w none -x --xyz --processes 5 --profile=mercator --tilesize 256 -z ' + i + ' ' + tifByte + ' ' + tilesForScene + ' & '
  }
  let fullCmd = cmd1 + ';' + cmd2
  start ('echo \'' + fullCmd + '\' >> ' + tilesRoot + '/cmd.sh') 
  tiler.run(fullCmd)
}

function cutBand(band, cut) {
  let ret = {}
  if (!band) return ret
  let rmin = 62000, rmax = 0
  let hist = {}
  for (let i=0; i<band.length; i++) {
    if (band[i] != 0 && rmin > band[i]) rmin = band[i]
    if (band[i] != 0 && rmax < band[i]) rmax = band[i]
    if (band[i] != 0) {
      let key = band[i]
      let v = hist[key]
      if (!v) v = 1 
      else v++
      hist[key] = v
    }
  }
  ret['min'] = rmin
  ret['max'] = rmax

  // вся площадь
  let s = 0
  let revBrightAr = []
  let dirBrightAr = []
  for (let bright in hist) {
    s += bright * hist[bright]
    dirBrightAr.push(bright)
    revBrightAr.unshift(bright)
  }
  let cutS = 0
  let leftCut = 0
  let rightCut = 0

  cutS = 0
  for (let bright of dirBrightAr) {
    cutS += bright * hist[bright]
    if (cutS/s > cut) { leftCut = bright; break}
  }
  ret['minCut'] = leftCut
  cutS = 0
  for (let bright of revBrightAr) {
    cutS += bright * hist[bright]
    if (cutS/s > cut) { rightCut = bright; break}
  }
  ret['maxCut'] = rightCut

  return ret
}

async function getProductTile(z,x,y,productId) {
  try {
    // находим пути к продуктам L2
    let pathL2 = await getPathArchiveL2(productId)
    // для тайлов сделаем отдельный корень tiles 
    // если маршрут запускался (есть папка) 
    let tilesProductRoot = config.archiveL2 + '/' + 'tiles' + '/' + productId
    // если 

    if (fs.existsSync(tilesProductRoot))   {
      // если уже был запрос тайла - вернем готовый
      let resFile = config.tmpDir + '/' + productId + '_' + z + '_' + x + '_' + y + '.png'

      // несколько сцен могут давать данные для одного тайла
      // ищем все папки для уровня z
      let arZ = await readdirpPromiseArray(tilesProductRoot, { type: 'directories',  depth: 1, directoryFilter: [ 'SCN*', z ] })
      // смотрим конкр-й тайл
      let arNeed = []
      for (let zPath of arZ) {
        let f = zPath.fullPath + '/' + x + '/' + y + '.png'
        if (fs.existsSync(f))  
          arNeed.push(f)
      }
      if (arNeed.length == 0) return null
      else  {
        // запросы синхронные - поэтому можно в один файл
        let tmpFile = config.tmpDir + '/tmp.png'
        images(arNeed[0]).save(tmpFile)
        for (let i=1; i<arNeed.length; i++) {
          images(tmpFile).draw(images(arNeed[i]), 0, 0).save(tmpFile);
        }
        // 
        fs.renameSync(tmpFile,resFile)
      }

      if (resFile) {
        return fs.readFileSync(resFile)
      }
    }

    try {
      // сразу создаем директорию как факт того, что запускали тайлер, иначе будут дублироваться
      start('mkdir -p ' + tilesProductRoot)

      // сцены от всех продуктов здесь вместе
      // сначала ищем все директории, которые начинаются с productId 
      let productL2 = getFolderMaskForScene(productId)
      let arProduct = await readdirpPromiseArray(pathL2, { type: 'directories', directoryFilter: [ productL2 + '*' ] })
      if (arProduct.length == 0) 
        console.log('Не найдены папки ' + productL2 + '*' + 'в корне ' + pathL2)
      // и в каждой директории тиф файл
      for (let prod of arProduct) { 
        console.log(prod.fullPath)
        let arTif = await readdirpPromiseArray(prod.fullPath, { type: 'files', fileFilter: [ '*.tif' ] })
        if (!arTif || arTif.length == 0) return null
        let arFile = []
        for(let tif of arTif) {
          // формируем тайлы
          makeTiles(tif.fullPath, tilesProductRoot, z)
        }
      }
    }
    catch(e) {
      console.log(e)
      start('rm -r ' + tilesProductRoot)
    }  
  }
  catch(e) {
    console.log(e)
  }
}

async function getCogTile(z,x,y,arProduct) {
  try {
    if (arProduct.length == 0) 
      return
    // и в каждой директории тиф файл для сцены
    //arProduct.sort((a,b) => a.path > b.path)
    let arTile = []
    for (let prod of arProduct) { 
      //let k = prod.fullPath.lastIndexOf('/')
      //let scn = prod.fullPath.substring(k+1)
      //let tif = prod.fullPath + '/' + scn + '.tif'
      let tif = prod.fullPath
      // формируем тайлы
      let tile = await readTile(tif, x, y, z, 256)
      if (tile )
        arTile.push(tile)
    }
    if (arTile.length == 1) 
      return await convertArray2Png(arTile[0])
    else if (arTile.length > 1) {
      // тайл попадает на несколько сцен - объединяем в одно изображение
      let bmp = arTile[0]
      for (let i=1; i<arTile.length; i++) {
        let newBmp = arTile[i]
        for (let k=0; k<bmp.length; k++) {
          if (newBmp[k] != 0) bmp[k] = newBmp[k]
        }
      }
      return await convertArray2Png(bmp)
    }
  }
  catch(e) {
    console.log(e)
  }
}


function runTiff() {
  return readTiff()
}

function readValue(path, file)  {
  try {
    let ret = ''
    if (!path) return [{data:''}]
    // Read the file:
    var data = fs.readFileSync(file, { encoding : 'UTF-8' });
    // Create an XMLDom Element:
    var doc = new xmldom.DOMParser().parseFromString(data);
    // Parse XML with XPath:
    let select = xpath.useNamespaces({qa:"http://schemas.niitp.ru/quality"})
    let value = select(path, doc)
    if (value.length > 1) ret = value
    else if (value.length == 1 && value[0]) {
      let v = value[0].nodeValue
      let k = v.lastIndexOf('.')
      if (k != -1) v = v.substring(0,k) 
      ret = v
    }
    return ret
  }
  catch(e) {
    console.log(e)
    return ''
  }
}

function readConfig(path, file, level)  {
  if (!fs.existsSync(file)) 
    return ''
  // Read the file:
  var data = fs.readFileSync(file, { encoding : 'UTF-8' });
  // Create an XMLDom Element:
  var doc = new xmldom.DOMParser().parseFromString(data);
  let xp = xpath.select(path, doc);
  let ret = ''
  for (let i=0; i<xp.length; i++) {
    let data = xp[i].data
    if (data) {
      if (level && level == 'L3M' && data.includes('tamarinArktur')) continue
      if (level && level == 'L3C' && data.includes('tamarinLeto')) continue
    }
    else {
      data = doc.toString()
    }
    ret += data +'\n'
  }
  return ret
}


function testQA() {
  let path = "/qa:qualityAnalysis/qa:result/qa:maskList/qa:mask[qa:type='Cloud']/qa:areaPercentage/text()"
  //path = "//qa:areaPercentage"
 
  let file = '/mnt/storagefs/redoute/interim/krimnosea_kv_pms_0c753c1c-d53e-44c3-a4c7-d22d0b6e384b/704/KV5_15704_12822-00_KANOPUS_20211026_083050_82.L1.PMS/KV5_15704_12822-00_KANOPUS_20211026_083050_82.L1.PMS.QA.xml'
  // Read the file:
  var data = fs.readFileSync(file, { encoding : 'UTF-8' });
  // Create an XMLDom Element:
  var doc = new xmldom.DOMParser().parseFromString(data);
  // Parse XML with XPath:
  let qa = xpath.useNamespaces({qa:"http://schemas.niitp.ru/quality"})
  var value = qa(path, doc);
}

function brief(field,code) {
  if (allCode[field] && allCode[field][code])
    return allCode[field][code]
  return null
}

async function readdirpPromiseArray(root, options) {
  let ret = await readdirp.promise(root,options)
  return ret
}



server()//
//makeProductFromFolder()

// sudo netstat -tulpn | grep :8081
// find /mnt/storagefs/TEST/STAND_REDOUTE/202312_ok/ -name forpost -type f | sed 's|\(.*\)|/opt/tamarin/runTamarin.sh tamarinForpost -p -cf /mnt/workfs/apoi/config/production/configForpost.xml -i \1|'


/*



SELECT dblink_connect('sverdlovsk', 'host=10.54.24.14 port=5432 user=postgres password=Passw0rd dbname=catalog_l0')

SELECT * FROM dblink('sverdlovsk', 'SELECT "productId" FROM sverdlovsk') AS t("productId" text);


SELECT distinct t."productId" FROM dblink('catalog_l0', 'SELECT "productId" FROM sverdlovsk') AS t("productId" text) inner join "PM" t2 on substring(t2."productId" from '(.*)\.L2') = t."productId"

select "productId" as product, 
max(SQRT(t1.value_vector_val[1]*t1.value_vector_val[1]+t1.value_vector_val[2]*t1.value_vector_val[2])) as frame,
max(SQRT(t2.value_vector_val[1]*t2.value_vector_val[1]+t2.value_vector_val[2]*t2.value_vector_val[2])) as retina,
max(SQRT(t3.value_vector_val[1]*t3.value_vector_val[1]+t3.value_vector_val[2]*t3.value_vector_val[2])) as band
from "PM" t
	inner join "PM_qa_result_geom_frame_shiftAbsMean" t1 on t."productId"=t1.object_id
	inner join "PM_qa_result_geom_retina_shiftAbsMean" t2 on t."productId"=t2.object_id
	inner join "PM_qa_result_geom_band_shiftAbsMean" t3 on t."productId"=t3.object_id
  where "productId" in ('KV3_13074_10294-00_KANOPUS_20200611_061112_7.L2.PMS.SCN16','KV3_15473_12422-00_KANOPUS_20201116_063234_35.L2.PMS.SCN22','KV3_18100_14723-00_KANOPUS_20210508_065222_83.L2.PMS.SCN17','KV3_18768_15343-01_KANOPUS_20210621_063842_7.L2.PMS.SCN29','KV3_19102_15609-02_KANOPUS_20210713_063125_7.L2.PMS.SCN21','KV3_20256_16706-01_KANOPUS_20210927_062048_7.L2.PMS.SCN12','KV3_20499_16973-03_KANOPUS_20211013_062420_15.L2.PMS.SCN02','KV3_24646_23210-01_KANOPUS_20220713_064636_8.L2.PMS.SCN32','KV3_30315_30982-00_KANOPUS_20230721_061008_17.L2.PMS.SCN25','KV4_08658_06041-00_KANOPUS_20190825_064214_7.L2.PMS.SCN11','KV4_12863_09338-01_KANOPUS_20200528_062453_7.L2.PMS.SCN35','KV4_12878_09347-01_KANOPUS_20200529_060810_83.L2.PMS.SCN11','KV4_13288_09712-00_KANOPUS_20200625_061654_83.L2.PMS.SCN04','KV4_14199_10534-01_KANOPUS_20200824_062642_9.L2.PMS.SCN29','KV4_14776_10979-00_KANOPUS_20201001_063146_83.L2.PMS.SCN01','KV4_18420_14179-00_KANOPUS_20210529_062447_7.L2.PMS.SCN24','KV4_18572_14312-00_KANOPUS_20210608_063849_7.L2.PMS.SCN01','KV4_18921_14598-00_KANOPUS_20210701_061424_83.L2.PMS.SCN23','KV4_19164_14786-00_KANOPUS_20210717_061728_7.L2.PMS.SCN12','KV4_19574_15157-02_KANOPUS_20210813_061620_83.L2.PMS.SCN27','KV4_24632_22089-01_KANOPUS_20220712_062830_7.L2.PMS.SCN23','KV4_24647_22133-01_KANOPUS_20220713_060903_8.L2.PMS.SCN25','KV4_30058_29408-02_KANOPUS_20230704_061912_9.L2.PMS.SCN19','KV4_30134_29517-00_KANOPUS_20230709_061548_8.L2.PMS.SCN30','KV4_30697_30337-01_KANOPUS_20230815_065022_9.L2.PMS.SCN13','KV5_03116_02379-00_KANOPUS_20190720_061353_8.L2.PMS.SCN28','KV5_03359_02538-03_KANOPUS_20190805_062437_8.L2.PMS.SCN20','KV5_03359_02538-03_KANOPUS_20190805_062437_8.L2.PMS.SCN21','KV5_08141_06202-00_KANOPUS_20200615_061758_9.L2.PMS.SCN22','KV5_08642_06574-00_KANOPUS_20200718_061800_7.L2.PMS.SCN31','KV5_08809_06722-00_KANOPUS_20200729_061839_14.L2.PMS.SCN23','KV5_13167_10445-00_KANOPUS_20210512_065846_83.L2.PMS.SCN15','KV5_13926_11126-01_KANOPUS_20210701_063348_14.L2.PMS.SCN04','KV5_13926_11126-01_KANOPUS_20210701_063348_14.L2.PMS.SCN05','KV5_14260_11418-02_KANOPUS_20210723_062746_7.L2.PMS.SCN14','KV5_14260_11418-02_KANOPUS_20210723_062746_7.L2.PMS.SCN17','KV5_15247_12369-00_KANOPUS_20210926_061920_8.L2.PMS.SCN21','KV5_18953_17626-01_KANOPUS_20220528_061744_8.L2.PMS.SCN41','KV5_20047_19243-04_KANOPUS_20220808_062230_7.L2.PMS.SCN12','KV5_20123_19360-02_KANOPUS_20220813_062423_7.L2.PMS.SCN11','KV5_20199_19470-03_KANOPUS_20220818_062722_7.L2.PMS.SCN32','KV5_24318_24963-01_KANOPUS_20230516_063355_28.L2.PMS.SCN03','KV5_25367_26505-01_KANOPUS_20230724_062739_17.L2.PMS.SCN48','KV6_03238_02315-01_KANOPUS_20190728_063010_7.L2.PMS.SCN02','KV6_03238_02315-01_KANOPUS_20190728_063010_7.L2.PMS.SCN03','KV6_08521_06388-00_KANOPUS_20200710_062315_3.L2.PMS.SCN02','KV6_08688_06522-00_KANOPUS_20200721_062339_83.L2.PMS.SCN04','KV6_09250_07006-01_KANOPUS_20200827_064915_7.L2.PMS.SCN30','KV6_09432_07157-00_KANOPUS_20200908_063119_7.L2.PMS.SCN34','KV6_09751_07416-00_KANOPUS_20200929_064656_29.L2.PMS.SCN13','KV6_09751_07416-00_KANOPUS_20200929_064656_29.L2.PMS.SCN36','KV6_09751_07416-00_KANOPUS_20200929_064656_29.L2.PMS.SCN39','KV6_12772_10008-00_KANOPUS_20210416_060207_7.L2.PMS.SCN13','KV6_13334_10518-00_KANOPUS_20210523_061818_20.L2.PMS.SCN13','KV6_13410_10587-00_KANOPUS_20210528_062349_8.L2.PMS.SCN22','KV6_13486_10654-00_KANOPUS_20210602_063054_9.L2.PMS.SCN25','KV6_13729_10857-01_KANOPUS_20210618_063414_14.L2.PMS.SCN11','KV6_13729_10857-01_KANOPUS_20210618_063414_14.L2.PMS.SCN12','KV6_13729_10857-01_KANOPUS_20210618_063414_14.L2.PMS.SCN17','KV6_13744_10872-01_KANOPUS_20210619_061706_7.L2.PMS.SCN02','KV6_13911_11019-00_KANOPUS_20210630_061336_7.L2.PMS.SCN24','KV6_14154_11215-01_KANOPUS_20210716_061852_83.L2.PMS.SCN09','KV6_14549_11558-00_KANOPUS_20210811_063325_7.L2.PMS.SCN31','KV6_19789_18670-03_KANOPUS_20220722_061356_15.L2.PMS.SCN22','KV6_19865_18767-02_KANOPUS_20220727_061732_9.L2.PMS.SCN07','KV6_24972_25572-00_KANOPUS_20230628_062239_83.L2.PMS.SCN08','KV6_25124_25791-00_KANOPUS_20230708_061738_17.L2.PMS.SCN01','KVI_10946_07487-01_KANOPUS_20190705_062456_9.L2.PMS.SCN08','KVI_11280_07722-00_KANOPUS_20190727_062652_9.L2.PMS.SCN32','KVI_16578_12195-00_KANOPUS_20200710_060502_28.L2.PMS.SCN18','KVI_16654_12254-00_KANOPUS_20200715_061359_7.L2.PMS.SCN14','KVI_21923_16750-01_KANOPUS_20210627_064829_83.L2.PMS.SCN15','KVI_22196_16979-00_KANOPUS_20210715_061520_82.L2.PMS.SCN29','KVI_22515_17247-00_KANOPUS_20210805_062513_8.L2.PMS.SCN14','KVI_27345_23896-03_KANOPUS_20220619_063347_9.L2.PMS.SCN10')
  group by "productId"
  order by "productId"
  
*/
