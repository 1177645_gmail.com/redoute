import { exec, execSync } from 'child_process'


export class Tiler {

    constructor() {
        this.queue = []
        this.started = false
        setInterval(() => {
            if (this.started) {
                return 
            }
            if (this.queue.length > 0) {
                this.started = true
                console.log(this.queue[0])
                exec(this.queue[0],(error, stdout, stderr) => {
                    this.queue.splice(0,1)
                    this.started = false
                    if (error) {
                      console.log(`${error}`);
                      return;
                    }
                })
            }
        },5000)
    }

    run(cmd) {
        if (this.queue.includes(cmd))
            return
        this.queue.push(cmd)
    }
}
