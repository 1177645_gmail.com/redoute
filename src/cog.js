import { Jimp } from 'jimp';
import { fromFile  } from 'geotiff';
import proj4 from 'proj4';
import proj4List from 'proj4-list'

function tileToLng (x, z) {
  return x * 360 / Math.pow(2,z) - 180;
}

function tileToLat (y, z) {
  return Math.atan(Math.sinh(Math.PI - y * 2 * Math.PI / Math.pow(2,z))) * (180 / Math.PI);
}

function epsg4326toEpsg3857(coordinates) {
  let x = (coordinates[0] * 20037508.34) / 180;
  let y =
    Math.log(Math.tan(((90 + coordinates[1]) * Math.PI) / 360)) /
    (Math.PI / 180);
  y = (y * 20037508.34) / 180;
  return [x, y];
}

let fileCount  = 0
let mapTiff = new Map()

async function readFoo() {

}

export async function readTile(tiffPath, x, y, z, size) {
  let raster = null
  let tiff = null
  try {
    tiff = await fromFile(tiffPath);

    /* 
    в момент выполнения await fromFile(tiffPath);
    происходит похоже чтение из очереди заданий и сразу попадаем на tiff = mapTiff.get(tiffPath)
    то есть по сути мап не работает

    tiff = mapTiff.get(tiffPath)
    if (!tiff) {
      console.log('Read tile ', '\t', tiffPath)
      await readFoo()
      tiff = await fromFile(tiffPath);
      if (mapTiff.size > 100) {
        for (let key of mapTiff.keys()) {
          let t = mapTiff.get(key)
          if (t) t.close()
        }
        mapTiff.clear()
      }
      mapTiff.set(tiffPath, tiff)
    }
    */
    if (x <= 2 || y <= 2) return raster

    let bbox = await makeBBox(tiff, x, y, z)
    if (!bbox) return null
    raster = await tiff.readRasters({
        bbox,
        height: size,
        width: size,
        interleave: true
    })
  }
  catch(e) {
    console.log(e)
  }
  if (tiff) tiff.close()

  return raster
}

async function makeBBox(tiff, x, y, z) {

  let bbox1 = await reproject(tiff,x,y,z)
  //let bbox2 = await reproject2(x,y,z)

  return bbox1
}

async function reproject(tiff,x,y,z) {
  let image = await tiff.getImage() 
  let geo = image.getGeoKeys()
  let code = null
  if (geo) {
    if (geo.ProjectedCSTypeGeoKey)
      code = 'EPSG:' + geo.ProjectedCSTypeGeoKey
    else if (geo.GeographicTypeGeoKey)
      code = 'EPSG:' + geo.GeographicTypeGeoKey
  }

  if (code == null) 
    throw 'Не удалось определить EPSG код проекции для ' + tiff.source.path

  let found = proj4List[code]
  if (found)
    proj4.defs([found]);
  else 
    return  null

  let x1 = tileToLng(x, z);
  let y1 = tileToLat(y, z);
  let xR = Number(x)+1
  let yR = Number(y)+1
  let x2 = tileToLng(xR, z);
  let y2 = tileToLat(yR, z);

  let n1 = proj4('EPSG:4326', code, [x1,y1])
  let n2 = proj4('EPSG:4326', code, [x2,y2])

  let bbox = [Math.min(n1[0],n2[0]), Math.min(n1[1],n2[1]), Math.max(n1[0],n2[0]), Math.max(n1[1],n2[1])];
  return bbox
}

async function reproject2(x,y,z) {
  let x1 = tileToLng(x, z);
  let y1 = tileToLat(y, z);
  let srs = epsg4326toEpsg3857([x1,y1])
  x1 = srs[0]
  y1 = srs[1]
  let xright = Number(x)+1
  let yright = Number(y)+1
  let x2 = tileToLng(xright, z);
  let y2 = tileToLat(yright, z);
  srs = epsg4326toEpsg3857([x2,y2])
  x2 = srs[0]
  y2 = srs[1]
  let bbox = [Math.min(x1,x2), Math.min(y1,y2), Math.max(x1,x2), Math.max(y1,y2)];
  return bbox
}

export async function convertArray2Png(bmp) {
  let bandCount = bmp.length/bmp.width/bmp.height
  let imageData = new Uint8Array(bmp.width * bmp.height * 4);
  for (let i = 0, j = 0; i < imageData.length; i += 4 ) {
    let alpha = 0
    for (let k=0; k<bandCount; k++) {
      imageData[i+k] = bmp[j+k];
      alpha += imageData[i+k]
    }
    for (let x=bandCount; x<3; x++) {
      imageData[i+x] = bmp[j]
    }
    imageData[i + 3] = alpha === 0 ? 0 : 255;
    j += bandCount;
  }
  let jimp =  new Jimp({data: imageData, width: bmp.width, height: bmp.height})
  let png =  await jimp.getBuffer('image/png')
  return png
}

async function main() {

    let tiff = await fromFile('/mnt/beegfs/redoute/dev3/archive_l2/2024/04/18/KV4_34465_35208-00_KANOPUS_20240418_222034_7.L2.PMS.SCN09/KV4_34465_35208-00_KANOPUS_20240418_222034_7.L2.PMS.SCN09.tif');
    let image = await tiff.getImage() 
    let geo = image.getGeoKeys()
    console.log('\t proj = ' + geo.ProjectedCSTypeGeoKey)
    return 'OK'
}

//main()

// представление проекций в разных формах
// https://spatialreference.org/ref/epsg/?search=3995